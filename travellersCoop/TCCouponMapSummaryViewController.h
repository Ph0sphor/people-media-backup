//
//  TCCouponMapSummaryViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "TCAppDelegate.h"
#import "CouponObject.h"
#import "CategoryObject.h"
#import "TCStoreLocation.h"
#import "TCCouponListPopoverViewController.h"
#import "SliderCategoryButtonView.h"
#import "FXBlurView.h"

@interface TCCouponMapSummaryViewController : UIViewController <MKMapViewDelegate, UIPopoverControllerDelegate, TCCouponListPopoverViewControllerDelegate, SliderCategoryButtonViewDelegate>
{
	IBOutlet MKMapView *mapView;
	__strong NSArray *annotationViewArray;
	__strong NSArray *couponArray;
	BOOL userLocationFlag;
	BOOL loaded;
	IBOutlet UIView *sliderView;
	IBOutlet FXBlurView *blurView;
	CGRect closedFrame;
	
	__strong UIPanGestureRecognizer *panRecognizer;
}
@property (nonatomic, strong) NSArray *categoryArray;
@property (nonatomic, strong) NSArray *storeArray;
@property (nonatomic, strong) NSMutableArray *categoryButtonArray;
@property (nonatomic, strong) TCCouponListPopoverViewController *popoverController;
@property (nonatomic, strong) CouponObject *tempCoupon;
@property (nonatomic, strong) UIColor *tempColor;
-(IBAction)toggleSlider:(id)sender;
-(IBAction)back:(id)sender;
-(void)setCouponArray:(NSArray *)coupons;
@end
