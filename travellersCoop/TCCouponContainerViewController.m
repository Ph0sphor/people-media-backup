//
//  TCCouponContainerViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 27/11/13.
//  Copyright (c) 2013 Judson Steel. All rights reserved.
//

#import "TCCouponContainerViewController.h"

@interface TCCouponContainerViewController ()

@end

@implementation TCCouponContainerViewController

-(IBAction)back:(id)sender
{
    if (self.couponVC != nil)
	{
		if ([self.couponVC back] == YES)
		{
			[[self navigationController] popViewControllerAnimated:YES];
		}
	}
	else
	{
		[[self navigationController] popViewControllerAnimated:YES];
	}
}

-(IBAction)help:(id)sender
{
	[self performSegueWithIdentifier:@"showHelp" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"embedSegue"])
	{
		self.couponVC = segue.destinationViewController;
		[self.couponVC setMyCoupon:self.myCoupon];
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

@end
