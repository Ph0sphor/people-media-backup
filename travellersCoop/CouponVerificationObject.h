//
//  CouponVerificationObject.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 31/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CouponVerificationObject : NSObject <NSCoding>

@property (nonatomic, strong) NSString * couponID;
@property (nonatomic, strong) NSString * categoryID;
@property (nonatomic, strong) NSDate * timestamp;
@property (nonatomic, strong) NSString * valueUsed;
@property (nonatomic, strong) NSString * storeCode;
@property (nonatomic, strong) NSString * storeID;
@property (nonatomic, assign) BOOL isSingleUse;

@end
