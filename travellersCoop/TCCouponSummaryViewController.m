//
//  TCCouponSummaryViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/07/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCCouponSummaryViewController.h"
#import "TCCouponContainerViewController.h"
#import "TCOfferTableViewCell.h"

@interface TCCouponSummaryViewController ()

@end

@implementation TCCouponSummaryViewController

-(IBAction)back:(id)sender
{
	[[self navigationController] popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)reloadData
{
	[couponTableView reloadData];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"showCoupon"])
	{
		TCCouponContainerViewController *detailVC = segue.destinationViewController;
		[detailVC setMyCoupon:self.tempCoupon];
		[detailVC setLabelColor:self.tempCoupon.categoryColor];
		[self setTempCoupon:nil];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	[categoryNameLabel setText:self.category.categoryName];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
	NSLog(@"count: %lu", (unsigned long)self.couponArray.count);
    return [self.couponArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"OfferCellIdentifier";
	TCOfferTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	CouponObject *coupon = [self.couponArray objectAtIndex:indexPath.row];
	[cell.titleLabel setText:coupon.couponTitle];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	if (indexPath.row % 2 == 0)
	{
		[cell.bgImage setImage:[UIImage imageNamed:@"light-yellow-1.png"]];
	}
	else
	{
		[cell.bgImage setImage:[UIImage imageNamed:@"light-yellow-2.png"]];
	}
	
	return cell;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	CouponObject *coupon = [self.couponArray objectAtIndex:indexPath.row];
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Offer - %@", [coupon couponTitle]]];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	
	TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate sendUbiquityEvent:[NSString stringWithFormat:kOfferViewedEvent, [coupon couponTitle]] withDescription:kOfferViewedEventDescription];
	
	self.tempCoupon = coupon;
	[self performSegueWithIdentifier:@"showCoupon" sender:self];
}



@end
