//
//  TCCouponDetailViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 16/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "TCCouponDetailViewController.h"
#import "CouponObject.h"
#import "StoreObject.h"
#import "TCMapViewController.h"
#import "TCAppDelegate.h"
#import "TCSavedView.h"
#import "TCHelpViewController.h"
#import "CouponVerificationObject.h"
#import "TCCouponContainerViewController.h"
#import "TCStoreTableViewCell.h"

@interface TCCouponDetailViewController ()

@end

@implementation TCCouponDetailViewController
@synthesize contentView, myCoupon;

-(BOOL)back
{
	return YES;
}

-(IBAction)showTermsAndConditions:(id)sender
{
	[detailScrollView scrollRectToVisible:CGRectMake(0, contentView.frame.size.height - 5, contentView.frame.size.width, 5) animated:YES];
}

-(IBAction)help:(id)sender
{
	[self performSegueWithIdentifier:@"showHelp" sender:self];
}

-(IBAction)callCompany:(id)sender
{
	// May return nil if a tracker has not already been initialized with a
	// property ID.
	id tracker = [[GAI sharedInstance] defaultTracker];
	// This screen name value will remain set on the tracker and sent with
	// hits until it is set to a new value or to nil.
	[tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Offer - %@ : Click to Call", self.myCoupon.couponTitle]];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"click-­to-­call"			// Event category (required)
														  action:@"offer"					// Event action (required)
														   label:self.myCoupon.couponTitle	// Event label
														   value:nil] build]];

	
    NSString *phoneString = [[[self.myCoupon.storeArray objectAtIndex:storeIndex] storePhone] stringByReplacingOccurrencesOfString:@" " withString:@"-"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@", phoneString]]];
}

-(IBAction)showCompanyOnMap:(id)sender
{
    NSLog(@"showCompanyOnMap");
	UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
	TCMapViewController *mapView = [storyboard instantiateViewControllerWithIdentifier:@"TCMapViewControllerIdentifier"];
	[mapView setMyStore:[self.myCoupon.storeArray objectAtIndex:storeIndex]];
	[mapView setMyCoupon:self.myCoupon];
	[mapView setCategoryID:[self.myCoupon.categoryID intValue]];
	[mapView setModalPresentationStyle:UIModalPresentationFullScreen];
	[mapView setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
	TCAppDelegate *sharedAppDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
    [sharedAppDelegate.navController presentViewController:mapView animated:YES completion:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"showMap"])
	{
		TCMapViewController *mapView = segue.destinationViewController;
		[mapView setMyStore:[self.myCoupon.storeArray objectAtIndex:storeIndex]];
		NSLog(@"store: %@", [self.myCoupon.storeArray objectAtIndex:storeIndex]);
		[mapView setCategoryID:[self.myCoupon.categoryID intValue]];
	}
}

-(IBAction)viewCompanyWebsite:(id)sender
{
    //NSLog(@"viewCompanyWebsite");
	// May return nil if a tracker has not already been initialized with a
	// property ID.
	id tracker = [[GAI sharedInstance] defaultTracker];
	// This screen name value will remain set on the tracker and sent with
	// hits until it is set to a new value or to nil.
	[tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Offer - %@ : Click to Website", self.myCoupon.couponTitle]];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"click-­to-­website"		// Event category (required)
														  action:@"offer"					// Event action (required)
														   label:self.myCoupon.couponTitle	// Event label
														   value:nil] build]];
	
	NSString *urlString = [[self.myCoupon.storeArray objectAtIndex:storeIndex] storeWebsite];
	if (![urlString hasPrefix:@"http"])
	{
		urlString = [NSString stringWithFormat:@"http://%@", urlString];
	}
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
	return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    // Do any additional setup after loading the view from its nib.
    [detailTapImageView setAlpha:0];
	
	[detailImageView sd_setImageWithURL:[NSURL URLWithString:self.myCoupon.couponImageURL]];
//	[detailImageView setImageWithURL:[NSURL URLWithString:self.myCoupon.couponImageURL]];
	
	if (![[self.myCoupon couponDescription] isEqual:[NSNull null]])
	{
		[descriptionLabel setText:[self.myCoupon couponDescription]];
	}
	else
	{
		[descriptionLabel setText:@""];
	}
    
	storeTableView.tableFooterView = [[UIView alloc] init];
	
	if ((self.myCoupon.storeArray.count * storeTableView.rowHeight) < storeTableView.frame.size.height)
	{
		[storeInfoView setFrame:CGRectMake(storeInfoView.frame.origin.x, storeInfoView.frame.origin.y, storeInfoView.frame.size.width, ((self.myCoupon.storeArray.count * storeTableView.rowHeight) + storeTableView.frame.origin.y))];
		[storeTableView setFrame:CGRectMake(storeTableView.frame.origin.x, storeTableView.frame.origin.y, storeTableView.frame.size.width, storeInfoView.frame.size.height - storeTableView.frame.origin.y)];
	}
	
	
    CGSize labelSize = CGSizeMake(300, 300);
    CGSize theStringSize = [self.myCoupon.couponTermsConditions sizeWithFont:termsConditionsLabel.font constrainedToSize:labelSize lineBreakMode:termsConditionsLabel.lineBreakMode];
    
    if (theStringSize.height < 47.0)
    {
        theStringSize.height = 47.0;
    }
    
    termsConditionsLabel.frame = CGRectMake(termsConditionsLabel.frame.origin.x, termsConditionsLabel.frame.origin.y, theStringSize.width, theStringSize.height);
    termsConditionsLabel.text = self.myCoupon.couponTermsConditions;
	[termsConditionsLabel sizeToFit];
    [termsAndConditionsView setFrame:CGRectMake(termsAndConditionsView.frame.origin.x, (storeInfoView.frame.origin.y + storeInfoView.frame.size.height), termsAndConditionsView.frame.size.width, 24 + termsConditionsLabel.frame.size.height + 8)];
	[contentView setFrame:CGRectMake(0, 0, contentView.frame.size.width, termsAndConditionsView.frame.origin.y + termsAndConditionsView.frame.size.height)];
	[detailScrollView setContentOffset:CGPointMake(0, 0)];
	[detailScrollView addSubview:contentView];
	[detailScrollView setContentInset:UIEdgeInsetsZero];
	[detailScrollView setContentSize:contentView.frame.size];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	
	if (contentView.superview)
	{
		[contentView removeFromSuperview];
		[contentView setFrame:CGRectMake(0, 0, contentView.frame.size.width, termsAndConditionsView.frame.origin.y + termsAndConditionsView.frame.size.height)];
		[detailScrollView setContentOffset:CGPointMake(0, 0)];
		[detailScrollView addSubview:contentView];
		[detailScrollView setContentInset:UIEdgeInsetsZero];
		[detailScrollView setContentSize:contentView.frame.size];
	}
}

-(void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)dealloc
{
    self.contentView = nil;
    self.myCoupon = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - UIActionSheetDelegate Methods
-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex != actionSheet.cancelButtonIndex)
	{
		switch (buttonIndex) {
			case 0: // call store
				[self callCompany:nil];
				break;
			case 1: // visit website
				[self viewCompanyWebsite:nil];
				break;
			case 2: // show on map
				[self showCompanyOnMap:nil];
				break;
				
			default:
				break;
		}
	}
}

#pragma mark - UITableViewDataSource & UITableViewDelegate Methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.myCoupon.storeArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"storeCellIdentifier";
	TCStoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	StoreObject *store = [self.myCoupon.storeArray objectAtIndex:indexPath.row];
	[cell.storeTitleLabel setText:store.storeName];
	[cell.storePhoneLabel setText:store.storePhone];
	[cell.storeWebsiteLabel setText:store.storeWebsite];
	cell.selectionStyle = UITableViewCellSelectionStyleGray;
	if (store.storeSuburb != nil && ![store.storeSuburb isEqualToString:@""])
	{
		[cell.storeAddressLabel setText:[NSString stringWithFormat:@"%@,\n%@ %@", store.storeStreetAddress, store.storeCity, store.storePostcode]];
	}
	else
	{
		[cell.storeAddressLabel setText:[NSString stringWithFormat:@"%@,\n%@, %@ %@", store.storeStreetAddress, store.storeSuburb, store.storeCity, store.storePostcode]];
	}
	
	
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	storeIndex = indexPath.row;
	StoreObject *store = [self.myCoupon.storeArray objectAtIndex:storeIndex];
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:store.storeName
															 delegate:self
													cancelButtonTitle:@"Cancel"
											   destructiveButtonTitle:nil
													otherButtonTitles:@"Call Store", @"Visit Website", @"Show on map", nil];
	[actionSheet showInView:self.view];
	
}

@end
