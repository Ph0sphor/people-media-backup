//
//  TCHelpViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 1/07/13.
//  Copyright (c) 2013 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCHelpViewController : UIViewController
{
	IBOutlet UIScrollView *scrollView;
	IBOutlet UIView *contentView;
}
-(IBAction)back:(id)sender;
-(IBAction)callFreephone:(id)sender;
@end
