//
//  TCTermsAndConditionsViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCTermsAndConditionsViewController : UIViewController
{
	IBOutlet UIScrollView *scrollView;
	IBOutlet UIView *contentView;
}
-(IBAction)back:(id)sender;
@end
