//
//  TCCouponListTableViewCell.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 15/07/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCCouponListTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
@property (nonatomic, strong) IBOutlet UIImageView *thumbImage;
@end
