//
//  TCLoginViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCAppDelegate.h"

@interface TCLoginViewController : UIViewController
{
	IBOutlet UIView *containerView;
	IBOutlet UIView *welcomeView;
	IBOutlet UIView *policyView;
	IBOutlet UIView *messageView;
	IBOutlet UITextField *lastNameTextField;
	IBOutlet UITextField *registrationTextField;
	IBOutlet UITextField *policyNumberTextField;
	IBOutlet UIView *policyNotFoundView;
	IBOutlet UIImageView *orangeBgImageView;
	IBOutlet UIButton *backButton;
	IBOutlet UIButton *policyGoButton;
	
	BOOL didFailFirstAttempt;
	BOOL didFailSecondAttempt;
	
	UIView *currentlyShowingView;
	UITextField *activeTextField;
	BOOL shouldIgnoreAnimation;
	
	
	BOOL didCheckChangeover;
	IBOutlet UIView *changeoverView;
	IBOutlet UILabel *changeoverMessageLabel;
	NSURL *changeoverURL;
	
}
-(IBAction)back:(id)sender;
- (IBAction)login:(id)sender;
-(IBAction)loginWithCredentials:(id)sender;
-(IBAction)phoneCallCentre:(id)sender;
-(IBAction)launchURL:(id)sender;
@end
