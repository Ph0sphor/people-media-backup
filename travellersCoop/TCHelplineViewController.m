//
//  TCHelplineViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCHelplineViewController.h"

@interface TCHelplineViewController ()

@end

@implementation TCHelplineViewController

-(IBAction)back:(id)sender
{
	[[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)callAMI:(id)sender
{
	// May return nil if a tracker has not already been initialized with a
	// property ID.
	id tracker = [[GAI sharedInstance] defaultTracker];
	// This screen name value will remain set on the tracker and sent with
	// hits until it is set to a new value or to nil.
	[tracker set:kGAIScreenName value:@"Helpline : Click to Call"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"click-­to-­call"			// Event category (required)
														  action:@"helpline"				// Event action (required)
														   label:@"0800 505 511"			// Event label
														   value:nil] build]];
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://0800505511"]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Helpline"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
