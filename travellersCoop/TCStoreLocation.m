//
//  TCStoreLocation.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 21/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "TCStoreLocation.h"
#import "CouponObject.h"

@implementation TCStoreLocation
@synthesize coordinate, storeName, storeStreet, storeSuburb, storeRegion, coupons;

- (NSString *)subtitle
{
	return self.storeStreet;
}

- (NSString *)title
{
    return self.storeName;
}

-(CLLocationCoordinate2D)getCoordinate{
    return self.coordinate;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c
{
	
    self = [super init];
    if (self)
    {
        self.coordinate=c;
    }
	return self;
}

-(void)dealloc
{
    self.storeName = nil;
    self.storeStreet = nil;
    self.storeSuburb = nil;
    self.storeRegion = nil;
    self.coupons = nil;
}

@end
