//
//  TCCouponSummaryViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/07/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCAppDelegate.h"
#import "CategoryObject.h"
#import "CouponObject.h"

@interface TCCouponSummaryViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
	IBOutlet UITableView *couponTableView;
	IBOutlet UIView *couponTableHeaderView;
	IBOutlet UILabel *categoryNameLabel;
}
@property (nonatomic, strong) CategoryObject *category;
@property (nonatomic, strong) NSArray *couponArray;
@property (nonatomic, strong) CouponObject *tempCoupon;
-(void)reloadData;
@end
