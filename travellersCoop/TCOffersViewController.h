//
//  TCOffersViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "TCAppDelegate.h"

@interface TCOffersViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
	IBOutlet UITableView *offersTableView;
	__strong NSArray *categoryArray;
	
}
@property (nonatomic, strong) NSArray *storesArray;
@property (nonatomic, strong) CategoryObject *tempCategory;
-(IBAction)back:(id)sender;
-(void)setCategoryArray:(NSArray *)categories;
@end
