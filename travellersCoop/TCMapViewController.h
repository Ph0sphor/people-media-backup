//
//  TCMapViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 21/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <MapKit/MKAnnotation.h>

@class StoreObject;
@class CouponObject;

@interface TCMapViewController : UIViewController <MKMapViewDelegate>
{
    IBOutlet MKMapView *myMapView;
	BOOL userLocationFlag;
}
@property (nonatomic, strong) StoreObject *myStore;
@property (nonatomic, strong) CouponObject *myCoupon;
@property (nonatomic, assign) int categoryID;
-(IBAction)back:(id)sender;

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;
- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)amapView
                             centerCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel;
- (double)longitudeToPixelSpaceX:(double)longitude;
- (double)latitudeToPixelSpaceY:(double)latitude;
- (double)pixelSpaceXToLongitude:(double)pixelX;
- (double)pixelSpaceYToLatitude:(double)pixelY;
@end
