//
//  TCHelplineViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCHelplineViewController : UIViewController
-(IBAction)callAMI:(id)sender;
@end
