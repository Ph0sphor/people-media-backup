//
//  TCBranchFinderViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface TCBranchFinderViewController : UIViewController <MKMapViewDelegate, UITextFieldDelegate>
{
	IBOutlet MKMapView *branchMapView;
	IBOutlet UIView *branchSearchView;
	IBOutlet UITextField *branchSearchTextField;
	BOOL userLocationFlag;
}
@property (nonatomic, strong) NSArray *branchArray;
-(void)zoomToFitTheseAnnotations:(NSArray *)annotations;
- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated;
-(void)performSearch;
@end
