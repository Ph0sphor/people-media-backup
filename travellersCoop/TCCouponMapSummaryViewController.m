//
//  TCCouponMapSummaryViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCCouponMapSummaryViewController.h"
#import "TCCouponContainerViewController.h"

@interface TCCouponMapSummaryViewController ()

@end

@implementation TCCouponMapSummaryViewController
@synthesize popoverController;

static NSString * const GMAP_ANNOTATION_SELECTED = @"gMapAnnontationSelected";

-(IBAction)back:(id)sender
{
	for (MKAnnotationView *view in annotationViewArray)
	{
		if ([view observationInfo] != nil)
		{
			[view removeObserver:self forKeyPath:@"selected"];
		}
	}
	
	[[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)toggleSlider:(id)sender
{
	if (sliderView.frame.origin.y == closedFrame.origin.y)
	{
		// open slider view
		CGRect newFrame = sliderView.frame;
		newFrame.origin.y = self.view.frame.size.height - sliderView.frame.size.height;
		[UIView animateWithDuration:0.3
						 animations:^{
							 sliderView.frame = newFrame;
						 }
						 completion:^(BOOL finished) {
							 
						 }];
		
	}
	else
	{
		// close slider view
		[UIView animateWithDuration:0.3
						 animations:^{
							 sliderView.frame = closedFrame;
						 }
						 completion:^(BOOL finished) {
							 
						 }];
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"showCoupon"])
	{
		TCCouponContainerViewController *detailVC = segue.destinationViewController;
		[detailVC setMyCoupon:self.tempCoupon];
		[detailVC setLabelColor:self.tempColor];
		[self setTempColor:nil];
		[self setTempCoupon:nil];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	blurView.dynamic = YES;
	blurView.blurRadius = 40;
	[blurView removeFromSuperview];
	if (self.categoryButtonArray == nil)
	{
		NSMutableArray *array = [[NSMutableArray alloc] init];
		[self setCategoryButtonArray:array];
	}
	
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Offer Map"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	
	int row = 0;
	int column = 0;
	for (CategoryObject *category in self.categoryArray)
	{
		if (column == 2)
		{
			column = 0;
			row++;
		}
		NSArray *xib = [[NSBundle mainBundle] loadNibNamed:@"SliderCategoryButtonView" owner:self options:nil];
		SliderCategoryButtonView *buttonView = [xib objectAtIndex:0];
		[buttonView setDelegate:self];
		[buttonView setCategoryID:category.categoryID];
		[buttonView setCategoryColor:category.categoryColor];
		[[buttonView buttonTitleLabel] setText:category.categoryName];
		[[buttonView buttonImageView] setImage:nil];
		
		UIImage *image = [self maskColor:category.categoryColor withMask:[UIImage imageNamed:@"colour-icon-tick-box-mask-2.png"]];
		[[buttonView buttonImageView] setImage:image];
		[[buttonView buttonImageView] setBackgroundColor:[UIColor whiteColor]];
		[buttonView setFrame:CGRectMake((column * 160), (row * 30) + 90, 160, 30)];
		[sliderView addSubview:buttonView];
		[self.categoryButtonArray addObject:buttonView];
		column++;
	}
	
	CGRect frame = sliderView.frame;
	frame.size.height = [(SliderCategoryButtonView *)[self.categoryButtonArray lastObject] frame].origin.y + [(SliderCategoryButtonView *)[self.categoryButtonArray lastObject] frame].size.height + 20.0;
	frame.origin.y = self.view.frame.size.height - 40;
	sliderView.frame = frame;
	closedFrame = sliderView.frame;
	userLocationFlag = NO;
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if (panRecognizer == nil)
	{
		panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(didPan:)];
		panRecognizer.minimumNumberOfTouches = 1;
		panRecognizer.maximumNumberOfTouches = 1;
	}
	
	[sliderView addGestureRecognizer:panRecognizer];
}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	if (panRecognizer != nil)
	{
		[sliderView removeGestureRecognizer:panRecognizer];
	}
}

-(void)didPan:(UIPanGestureRecognizer *)sender
{
	if ([sender state] == UIGestureRecognizerStateChanged || [sender state] == UIGestureRecognizerStateEnded)
	{
		CGPoint translation = [sender translationInView:self.view];
		CGRect newFrame = sliderView.frame;
		newFrame.origin.y = newFrame.origin.y + translation.y;
		if (newFrame.origin.y < (self.view.frame.size.height - sliderView.frame.size.height))
		{
			newFrame.origin.y = self.view.frame.size.height - sliderView.frame.size.height;
		}
		if (newFrame.origin.y > closedFrame.origin.y)
		{
			newFrame.origin.y = closedFrame.origin.y;
		}
		sliderView.frame = newFrame;
		[sender setTranslation:CGPointZero inView:self.view];
	}
}

-(void)setCategoryWithID:(int)categoryID selected:(BOOL)selected
{
	NSLog(@"setCategoryWithID: %d, selected: %d", categoryID, selected);
	
	if (selected == YES)
	{
		for (StoreObject *store in self.storeArray)
		{
			TCStoreLocation *storeLocation = [[TCStoreLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake([[store storeLatitude] doubleValue], [[store storeLongitude] doubleValue])];
			storeLocation.storeName = [store storeName];
			storeLocation.storeStreet = [store storeStreetAddress];
			storeLocation.storeSuburb = [store storeSuburb ];
			storeLocation.storeRegion = [store storeCity];
			if (couponArray != nil)
			{
				NSMutableArray *storeCouponArray = [[NSMutableArray alloc] init];
				for (CouponObject *coupon in couponArray)
				{
					if ([coupon.storeArray containsObject:store] && [coupon.categoryID intValue] == categoryID)
					{
						[storeCouponArray addObject:coupon];
					}
				}
				storeLocation.coupons = storeCouponArray;
				
			}
			
			if (storeLocation.coupons != nil && storeLocation.coupons.count > 0)
			{
				[mapView addAnnotation:storeLocation];
			}
		}
	}
	else
	{
		for (id<MKAnnotation> annotation in mapView.annotations)
		{
			if ([annotation isKindOfClass:[TCStoreLocation class]])
			{
				NSMutableArray *newCouponArray = [[NSMutableArray alloc] init];
				for (CouponObject *coupon in [(TCStoreLocation *)annotation coupons])
				{
					if ([coupon.categoryID intValue] != categoryID)
					{
						[newCouponArray addObject:coupon];
					}
				}
				if (newCouponArray.count > 0)
				{
					[(TCStoreLocation *)annotation setCoupons:newCouponArray];
					[mapView removeAnnotation:annotation];
					[mapView addAnnotation:annotation];
				}
				else
				{
					[mapView removeAnnotation:annotation];
				}
			}
		}
	}
}

-(void)setCouponArray:(NSArray *)coupons
{
	couponArray = coupons;
	
	for (StoreObject *store in self.storeArray)
	{
		TCStoreLocation *storeLocation = [[TCStoreLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake([[store storeLatitude] doubleValue], [[store storeLongitude] doubleValue])];
		storeLocation.storeName = [store storeName];
		storeLocation.storeStreet = [store storeStreetAddress];
		storeLocation.storeSuburb = [store storeSuburb ];
		storeLocation.storeRegion = [store storeCity];
		if (couponArray != nil)
		{
			NSMutableArray *storeCouponArray = [[NSMutableArray alloc] init];
			for (CouponObject *coupon in couponArray)
			{
				if ([coupon.storeArray containsObject:store])
				{
					[storeCouponArray addObject:coupon];
				}
			}
			storeLocation.coupons = storeCouponArray;
			
		}
		
		if (storeLocation.coupons != nil && storeLocation.coupons.count > 0)
		{
			[mapView addAnnotation:storeLocation];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIImage *)maskColor:(UIColor *)color withMask:(UIImage *)maskImage
{
    if (color != nil)
	{
		const CGFloat *components = CGColorGetComponents([color CGColor]);
		CGFloat red = components[0];
		CGFloat green = components[1];
		CGFloat blue = components[2];
		CGFloat alpha = components[3];

		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = CGBitmapContextCreate(NULL, maskImage.size.width, maskImage.size.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipLast);
		
		CGContextSetRGBFillColor(context, red, green, blue, alpha );
		
		CGContextAddRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		CGContextFillRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		
		CGImageRef solidImage = CGBitmapContextCreateImage(context);
		
		CGColorSpaceRelease(colorSpace);
		CGContextRelease(context);
		
		CGImageRef maskRef = maskImage.CGImage;
		
		CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
											CGImageGetHeight(maskRef),
											CGImageGetBitsPerComponent(maskRef),
											CGImageGetBitsPerPixel(maskRef),
											CGImageGetBytesPerRow(maskRef),
											CGImageGetDataProvider(maskRef), NULL, false);
		
		CGImageRef masked = CGImageCreateWithMask(solidImage, mask);
		CGImageRelease(solidImage);
		CGImageRelease(mask);
		
		UIImage *returnImage = [UIImage imageWithCGImage:masked];
		CGImageRelease(masked);
		return returnImage;
	}
    
    return nil;
}

#pragma mark - Map View Methods
#define MERCATOR_RADIUS 85445659.44705395
#define MERCATOR_OFFSET 268435456
#define iphoneScaleFactorLatitude   9.0
#define iphoneScaleFactorLongitude  11.0
- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude
{
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX
{
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY
{
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
}

- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)amapView
                             centerCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel
{
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    // determine the scale value from the zoom level
    NSInteger zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = amapView.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated
{
    
    // clamp large numbers to 28
    zoomLevel = MIN(zoomLevel, 28);
    
    // use the zoom level to compute the region
    MKCoordinateSpan span = [self coordinateSpanWithMapView:mapView centerCoordinate:centerCoordinate andZoomLevel:zoomLevel];
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    // set the region like normal
    [mapView setRegion:region animated:animated];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if (userLocationFlag == NO)
	{
		[self setCenterCoordinate:userLocation.coordinate zoomLevel:10 animated:YES];
	}
	
	userLocationFlag = YES;
}

-(void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
	if (views.count > 0 && [[(MKAnnotationView *)[views objectAtIndex:0] annotation] isKindOfClass:[TCStoreLocation class]] == YES)
	{
		if (annotationViewArray != nil)
		{
			NSMutableArray *array = [NSMutableArray arrayWithArray:annotationViewArray];
			[array addObjectsFromArray:views];
			annotationViewArray = array;
		}
		else
		{
			annotationViewArray = views;
		}
	}
	
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    // if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
	else if ([annotation isKindOfClass:[TCStoreLocation class]])
    {
        MKAnnotationView *annotationView = nil;
        static NSString *ImageIdentifier = @"ImageIdentifier";
        MKAnnotationView *storeView = [mV dequeueReusableAnnotationViewWithIdentifier:ImageIdentifier];
        if (storeView == nil)
        {
            storeView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:ImageIdentifier];
        }
        
        storeView.enabled = YES;
        storeView.canShowCallout = NO;
        
		if ([[(TCStoreLocation *)annotation coupons] count] > 0)
		{
			UIImage *image = [self maskColor:[(CouponObject *)[[(TCStoreLocation *)annotation coupons] objectAtIndex:0] categoryColor]  withMask:[UIImage imageNamed:@"pinmask.png"]];
			[storeView setImage:image];
		}
		else
		{
			UIImage *image = [self maskColor:[UIColor yellowColor] withMask:[UIImage imageNamed:@"pinmask.png"]];
			[storeView setImage:image];
		}
		
        annotationView = storeView;
		
		
		if ([annotationView observationInfo] == nil)
		{
			[annotationView addObserver:self
							 forKeyPath:@"selected"
								options:NSKeyValueObservingOptionNew
								context:(__bridge void *)(GMAP_ANNOTATION_SELECTED)];
		}
		
        return annotationView;
    }
    else
    {
        return nil;
    }
}


- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context{
	NSString *action = (__bridge NSString*)context;
    if([action isEqualToString:GMAP_ANNOTATION_SELECTED])
    {
		if (self.popoverController != nil && self.popoverController.view.superview)
		{
			CGRect frame = self.popoverController.view.frame;
			frame.origin.y = self.view.frame.size.height;
			[UIView animateWithDuration:0.3
								  delay:0
								options:UIViewAnimationOptionCurveEaseInOut
							 animations:^{
								 [self.popoverController.view setFrame:frame];
								 [blurView setFrame:frame];
							 }
							 completion:^(BOOL finished) {
								 
								 BOOL annotationAppeared = [[change valueForKey:@"new"] boolValue];
								 if (annotationAppeared == YES)
								 {
									 [self.popoverController.view removeFromSuperview];
									 [blurView removeFromSuperview];
									 self.popoverController.delegate = nil;
									 self.popoverController = nil;
									 
									 UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
									 self.popoverController = [storyboard instantiateViewControllerWithIdentifier:@"CouponListPopover"];
									 self.popoverController.delegate = self;
									 [self.popoverController setCouponArray:[(TCStoreLocation *)[object annotation] coupons]];
									 
									 CGRect rect = self.popoverController.view.frame;
									 rect.origin.y = self.view.frame.size.height;
									 rect.size.height = [self.popoverController getNewTableHeight];
									 [self.popoverController.view setFrame:rect];
									 [blurView setFrame:rect];
									 [self.view addSubview:blurView];
									 [self.view addSubview:self.popoverController.view];
									 CGRect newFrame = self.popoverController.view.frame;
									 newFrame.origin.y = self.view.frame.size.height - self.popoverController.view.frame.size.height;
									 [UIView animateWithDuration:0.3
													  animations:^{
														  [self.popoverController.view setFrame:newFrame];
														  [blurView setFrame:newFrame];
													  }
													  completion:^(BOOL finished) {
														  
													  }];
								 }
							 }];
		}
		else
		{
			BOOL annotationAppeared = [[change valueForKey:@"new"] boolValue];
			if (annotationAppeared == YES)
			{
				UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
				self.popoverController = [storyboard instantiateViewControllerWithIdentifier:@"CouponListPopover"];
				self.popoverController.delegate = self;
				[self.popoverController setCouponArray:[(TCStoreLocation *)[object annotation] coupons]];
				NSLog(@"coupons: %@", self.popoverController.couponArray);
				CGRect rect = self.popoverController.view.frame;
				rect.origin.y = self.view.frame.size.height;
				rect.size.height = [self.popoverController getNewTableHeight];
				[self.popoverController.view setFrame:rect];
				[blurView setFrame:rect];
				[self.view addSubview:blurView];
				[self.view addSubview:self.popoverController.view];
				CGRect newFrame = self.popoverController.view.frame;
				newFrame.origin.y = self.view.frame.size.height - self.popoverController.view.frame.size.height;
				[self.popoverController reloadData];
				[UIView animateWithDuration:0.3
								 animations:^{
									 [self.popoverController.view setFrame:newFrame];
									 [blurView setFrame:newFrame];
								 }
								 completion:^(BOOL finished) {
									 
								 }];
			}
		}
	}
	
}

-(void)didSelectCoupon:(CouponObject *)coupon
{
	for (CouponObject *couponObj in couponArray)
	{
		if (couponObj.couponID == coupon.couponID)
		{
			id tracker = [[GAI sharedInstance] defaultTracker];
			[tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Offer - %@", [coupon couponTitle]]];
			[tracker send:[[GAIDictionaryBuilder createAppView] build]];
			
			TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
			[appDelegate sendUbiquityEvent:[NSString stringWithFormat:kOfferViewedEvent, [coupon couponTitle]] withDescription:kOfferViewedEventDescription];
			
			
			self.tempCoupon = couponObj;
			self.tempColor = couponObj.categoryColor;
			[self performSegueWithIdentifier:@"showCoupon" sender:self];
			break;
		}
	}
	
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    //self.tempCoupon = [(TCStoreLocation *)view.annotation myCoupon];
	//self.tempColor = [(CouponObject *)[(TCStoreLocation *)view.annotation myCoupon] categoryColor];
	[self performSegueWithIdentifier:@"showCoupon" sender:self];
}

-(void)zoomToFitMapAnnotations:(MKMapView*)aMapView
{
	
    if([aMapView.annotations count] == 0)
		return;
	
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
	
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
	
	
	
    for(TCStoreLocation* annotation in aMapView.annotations)
    {
        
		topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
		topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
		bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
		bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
	
	if (userLocationFlag == YES) {
		topLeftCoord.longitude = fmin(topLeftCoord.longitude, aMapView.userLocation.location.coordinate.longitude);
		topLeftCoord.latitude = fmax(topLeftCoord.latitude, aMapView.userLocation.location.coordinate.latitude);
		
		bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, aMapView.userLocation.location.coordinate.longitude);
		bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, aMapView.userLocation.location.coordinate.latitude);
	}
    
	
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
	if (region.center.latitude < -90)
	{
		region.center.latitude = -90;
	}
	if (region.center.latitude > 90)
	{
		region.center.latitude = 90;
	}
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
	
	if (region.center.longitude < -180)
	{
		region.center.longitude = -180;
	}
	if (region.center.longitude > 180)
	{
		region.center.longitude = 180;
	}
	
    region.span.latitudeDelta = fabs(topLeftCoord.latitude - bottomRightCoord.latitude); // Add a little extra space on the sides
    region.span.longitudeDelta = fabs(bottomRightCoord.longitude - topLeftCoord.longitude); // Add a little extra space on the sides
	
	
    region = [aMapView regionThatFits:region];
    [aMapView setRegion:region animated:YES];
}

@end
