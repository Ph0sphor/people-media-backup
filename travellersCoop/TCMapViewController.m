//
//  TCMapViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 21/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "TCMapViewController.h"
#import "StoreObject.h"
#import "CouponObject.h"
#import "TCStoreLocation.h"

#define MERCATOR_RADIUS 85445659.44705395
#define MERCATOR_OFFSET 268435456

#define iphoneScaleFactorLatitude   9.0    
#define iphoneScaleFactorLongitude  11.0 

@interface TCMapViewController ()

@end

@implementation TCMapViewController
@synthesize myStore, categoryID;
-(IBAction)back:(id)sender
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	userLocationFlag = NO;
    TCStoreLocation *storeLocation = [[TCStoreLocation alloc] initWithCoordinate:CLLocationCoordinate2DMake([[(StoreObject *)self.myStore storeLatitude] doubleValue], [[(StoreObject *)self.myStore storeLongitude] doubleValue])];
    storeLocation.storeName = self.myStore.storeName;
    storeLocation.storeStreet = self.myStore.storeStreetAddress;
    storeLocation.storeSuburb = self.myStore.storeSuburb;
    storeLocation.storeRegion = self.myStore.storeCity;
    [myMapView addAnnotation:storeLocation];
	
    [self setCenterCoordinate:storeLocation.coordinate zoomLevel:15 animated:YES];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

-(void)dealloc
{
    self.myStore = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(UIImage *)maskColor:(UIColor *)color withMask:(UIImage *)maskImage
{
	if (color != nil)
	{
		const CGFloat *components = CGColorGetComponents([color CGColor]);
		CGFloat red = components[0];
		CGFloat green = components[1];
		CGFloat blue = components[2];
		CGFloat alpha = components[3];
		
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = CGBitmapContextCreate(NULL, maskImage.size.width, maskImage.size.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipLast);
		
		CGContextSetRGBFillColor(context, red, green, blue, alpha );
		
		CGContextAddRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		CGContextFillRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		
		CGImageRef solidImage = CGBitmapContextCreateImage(context);
		
		CGColorSpaceRelease(colorSpace);
		CGContextRelease(context);
		
		CGImageRef maskRef = maskImage.CGImage;
		
		CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
											CGImageGetHeight(maskRef),
											CGImageGetBitsPerComponent(maskRef),
											CGImageGetBitsPerPixel(maskRef),
											CGImageGetBytesPerRow(maskRef),
											CGImageGetDataProvider(maskRef), NULL, false);
		
		CGImageRef masked = CGImageCreateWithMask(solidImage, mask);
		CGImageRelease(solidImage);
		CGImageRelease(mask);
		
		UIImage *returnImage = [UIImage imageWithCGImage:masked];
		CGImageRelease(masked);
		return returnImage;
	}
	
	return nil;
}

#pragma mark - map delegate methods
-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if (userLocationFlag == NO)
	{
		if (userLocation.location != nil)
		{
			userLocationFlag = YES;
			CLLocation *store = [[CLLocation alloc] initWithLatitude:[[(StoreObject *)self.myStore storeLatitude] doubleValue] longitude:[[(StoreObject *)self.myStore storeLongitude] doubleValue]];
			if ([store distanceFromLocation:userLocation.location] < 10000.0)
			{
				[self zoomToFitMapAnnotations:myMapView];
			}
		}
	}
}

- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    // if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
	else if ([annotation isKindOfClass:[TCStoreLocation class]])
    {
		MKAnnotationView *annotationView = nil;
		static NSString *ImageIdentifier = @"ImageIdentifier";
		MKAnnotationView *storeView = [mV dequeueReusableAnnotationViewWithIdentifier:ImageIdentifier];
		if (storeView == nil)
		{
			storeView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:ImageIdentifier];
		}
		
		UIImage *image = [self maskColor:[self.myCoupon categoryColor]  withMask:[UIImage imageNamed:@"pinmask.png"]];
		[storeView setImage:image];
		
		storeView.enabled = YES;
		storeView.canShowCallout = YES;
		UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
		storeView.rightCalloutAccessoryView = rightButton;
		
		annotationView = storeView;
		return annotationView;
    }
    else
    {
        return nil;
    }
}

- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude
{
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX
{
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY
{
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
} 

- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)amapView
                             centerCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel
{
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    // determine the scale value from the zoom level
    NSInteger zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = amapView.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
} 

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated
{
    // clamp large numbers to 28
    zoomLevel = MIN(zoomLevel, 28);
    
    // use the zoom level to compute the region
    MKCoordinateSpan span = [self coordinateSpanWithMapView:myMapView centerCoordinate:centerCoordinate andZoomLevel:zoomLevel];
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    // set the region like normal
    [myMapView setRegion:region animated:animated];
}

-(void)zoomToFitMapAnnotations:(MKMapView*)aMapView
{
	
    if([aMapView.annotations count] == 0)
		return;
	
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
	
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
	
	
	
    for(TCStoreLocation* annotation in aMapView.annotations)
    {
        
		topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
		topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
		bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
		bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
	
	if (userLocationFlag == YES)
	{
		topLeftCoord.longitude = fmin(topLeftCoord.longitude, aMapView.userLocation.location.coordinate.longitude);
		topLeftCoord.latitude = fmax(topLeftCoord.latitude, aMapView.userLocation.location.coordinate.latitude);
		
		bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, aMapView.userLocation.location.coordinate.longitude);
		bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, aMapView.userLocation.location.coordinate.latitude);
	}
    
	
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
	if (region.center.latitude < -90)
	{
		region.center.latitude = -90;
	}
	if (region.center.latitude > 90)
	{
		region.center.latitude = 90;
	}
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
	
	if (region.center.longitude < -180)
	{
		region.center.longitude = -180;
	}
	if (region.center.longitude > 180)
	{
		region.center.longitude = 180;
	}
	
    region.span.latitudeDelta = fabs((topLeftCoord.latitude - bottomRightCoord.latitude)  * 1.2); // Add a little extra space on the sides
    region.span.longitudeDelta = fabs((bottomRightCoord.longitude - topLeftCoord.longitude)  * 1.2); // Add a little extra space on the sides
	
	
    region = [aMapView regionThatFits:region];
    [aMapView setRegion:region animated:YES];
}

@end
