//
//  TCAppDelegate.m
//  travellersCoop
//
//  Created by Scott Judson on 7/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "TCAppDelegate.h"
#include<sys/xattr.h>
#import "TCMainMenuViewController.h"
#import "SDWebImage/SDWebImage/SDWebImageDownloader.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "CategoryObject.h"
#import "CouponObject.h"
#import "StoreObject.h"
#import "CouponVerificationObject.h"
#import "Reachability.h"
#import "SSZipArchive.h"
#import "AMIBranchObject.h"

#define kDefaultVersion @"title_string"

#define kAuthToken @"z2DJMD4V38Bt5zx!2s638Q4QV"

@implementation TCAppDelegate

@synthesize window = _window;
@synthesize navController, locationManager;
@synthesize isRetinaDisplay, isSendingCoupons, downloadingArray, downloaderArray;

static NSString *vendorID = @"18";
static NSString *baseURL = @"http://my.koup.in/ios/";

-(NSString *)getVendorID
{
	return vendorID;
}


static NSString *ubiqityFullURL = @"https://amiassist-staging.uq.co.nz/registerdevice/%@/iOS/%@?authToken=%@&surname=%@&identifierType=%@&identifier=%@";
static NSString *ubiqityEventURL = @"https://amiassist-staging.uq.co.nz/appendevent?authToken=%@&surname=%@&identifierType=%@&identifier=%@";
static NSString *ubiqityShortURL = @"https://amiassist-staging.uq.co.nz/registerdevice/%@/iOS/%@?authToken=%@";

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	gotToken = NO;
	
	[[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound];
	
	[UAPush shared].pushNotificationDelegate = self;
    UAConfig *config = [UAConfig defaultConfig];
	[UAirship takeOff:config];
	[UAPush shared].userPushNotificationsEnabled = YES;
	
	[UALocationService setAirshipLocationServiceEnabled:YES];
	[[[UAirship shared] locationService] setDelegate:self];
	UALocationService *locationService = [[UAirship shared] locationService];
	[locationService startReportingSignificantLocationChanges];
	[locationService reportCurrentLocation];
	
	currentLocation = nil;
	
    if (self.downloadingArray == nil)
    {
        NSMutableArray *newArray = [[NSMutableArray alloc] init];
        [self setDownloadingArray:newArray];
    }
    totalImages = 0;
	self.navController = (UINavigationController *)self.window.rootViewController;
    
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(displayLinkWithTarget:selector:)] &&
        ([UIScreen mainScreen].scale == 2.0))
    {
        [self setIsRetinaDisplay:YES];
    }
    else
    {
        [self setIsRetinaDisplay:NO];
    }
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
	
	[GAI sharedInstance].trackUncaughtExceptions = YES;
	[GAI sharedInstance].dispatchInterval = 10;
	self.tracker = [[GAI sharedInstance] trackerWithTrackingId:@"UA-2287058-6"];
	
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	[[self.navController topViewController] viewWillDisappear:NO];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
	[self.navController popToRootViewControllerAnimated:NO];
	[[self.navController topViewController] viewWillAppear:NO];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"] forKey:kDefaultVersion];
    [defaults synchronize];

	if ([CLLocationManager locationServicesEnabled])
    {
        if (self.locationManager == nil)
        {
            self.locationManager = [[CLLocationManager alloc] init];
            [self.locationManager setDelegate:self];
            [self.locationManager setDistanceFilter:100.0];
            [self.locationManager setDesiredAccuracy:kCLLocationAccuracyKilometer];
        }
        
        [self.locationManager startUpdatingLocation];
    }
	
	NSLog(@"authorization status: %d", [CLLocationManager authorizationStatus]);
	if ([CLLocationManager authorizationStatus]  == kCLAuthorizationStatusNotDetermined && [self.locationManager respondsToSelector:@selector(requestAlwaysAuthorization)])
	{
		[self.locationManager requestWhenInUseAuthorization];
		NSLog(@"Location status not determined");
	}
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Saves changes in the application's managed object context before the application terminates.
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
}

- (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL
{
    const char* filePath = [[URL path] fileSystemRepresentation];   
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
    
}

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
		   fromLocation:(CLLocation *)oldLocation
{
    NSDate *dateNow = [NSDate date];
    NSTimeInterval interval = [dateNow timeIntervalSinceDate:newLocation.timestamp];
    if (interval < 300)
    {
		currentLocation = newLocation;
		NSLog(@"currentLocation = %@", currentLocation);
    }
    else
    {
        [self.locationManager stopUpdatingLocation];
        [self.locationManager startUpdatingLocation];
    }
}

-(CLLocation *)getCurrentLocation;
{
	return currentLocation;
}

#pragma mark - Push Notifications Methods

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
	NSString * tokenAsString = [[[deviceToken description]
								 stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]]
								stringByReplacingOccurrencesOfString:@" " withString:@""];
	NSLog(@"didRegisterForRemoteNotificationsWithDeviceToken: %@", tokenAsString);
	[[NSUserDefaults standardUserDefaults] setObject:tokenAsString forKey:@"kNotificationToken"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	NSLog(@"APPLICATION: didReceiveRemoteNotification: %@",userInfo);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))completionHandler NS_AVAILABLE_IOS(7_0)
{
		NSLog(@"APPLICATION: didReceiveRemoteNotification:fetchCompletionHandler %@",userInfo);
	completionHandler(UIBackgroundFetchResultNoData);
}

- (void)receivedForegroundNotification:(NSDictionary *)notification
				fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
	
	NSLog(@"URBAN AIRSHIP: Received a notification while the app was already in the foreground");
	[self sendUbiquityEvent:kGeoPushTriggeredEvent withDescription:kGeoPushTriggeredEventDescription];
	
	// Be sure to call the completion handler with a UIBackgroundFetchResult
	completionHandler(UIBackgroundFetchResultNoData);
}

- (void)launchedFromNotification:(NSDictionary *)notification
		  fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
	
	NSLog(@"URBAN AIRSHIP: The application was launched or resumed from a notification");
	
	[self sendUbiquityEvent:kGeoPushTriggeredEvent withDescription:kGeoPushTriggeredEventDescription];
	
	// Be sure to call the completion handler with a UIBackgroundFetchResult
	completionHandler(UIBackgroundFetchResultNoData);
}

- (void)receivedBackgroundNotification:(NSDictionary *)notification
				fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
	
	// Do something with the notification in the background
	
	// Be sure to call the completion handler with a UIBackgroundFetchResult
	completionHandler(UIBackgroundFetchResultNoData);
}



#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
/*- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}*/

- (NSString *)applicationDocumentsDirectory
{
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
}

-(void)getCategories:(void (^)(NSArray *))completion
{
    ASIFormDataRequest *categoryRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *categoryRequest_b = categoryRequest;
    [categoryRequest setPostValue:@"categories" forKey:@"object"];
    [categoryRequest setPostValue:@"get_all" forKey:@"action"];
	[categoryRequest setPostValue:vendorID forKey:@"vendor_id"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [categoryRequest setPostValue:[defaults objectForKey:@"kpid"] forKey:@"kpid"];
    [categoryRequest setFailedBlock:^{
        //[self.mainMenuViewController removeWaitView];
    }];
    [categoryRequest setCompletionBlock:^{
        NSError* error;
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[categoryRequest_b responseData] options:kNilOptions error:&error];
		//NSLog(@"categories: %@", dataArray);
		if (error != nil)
		{
			// ERROR
		}
        else
		{
			NSMutableArray *newCategoryArray = [[NSMutableArray alloc] init];
			for (NSDictionary *categoryDict in dataArray)
			{
				CategoryObject *newCategory = [[CategoryObject alloc] init];
				[newCategory setCategoryID:[[categoryDict objectForKey:@"id"] intValue]];
				[newCategory setCategoryName:[categoryDict objectForKey:@"name"]];
				[newCategory setParentID:[[categoryDict objectForKey:@"parent_id"] intValue]];
				[newCategory setVendorID:[[categoryDict objectForKey:@"vendor_id"] intValue]];
				if (![[categoryDict objectForKey:@"rgb"] isEqual:[NSNull null]] && [categoryDict objectForKey:@"rgb"] != nil)
				{
					NSString *rgbString = [categoryDict objectForKey:@"rgb"];
					NSArray *valueArray = [rgbString componentsSeparatedByString:@","];
					if(valueArray.count == 3)
					{
						float red = [[valueArray objectAtIndex:0] floatValue];
						float green = [[valueArray objectAtIndex:1] floatValue];
						float blue = [[valueArray objectAtIndex:2] floatValue];
						
						
						UIColor *color = [[UIColor alloc] initWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0];
						[newCategory setCategoryColor:color];
					}
				}
				
				
				
				
				[newCategoryArray addObject:newCategory];
			}
			NSArray *returnArray = [NSArray arrayWithArray:newCategoryArray];
			completion(returnArray);
		}
        
        
        
        
    }];
    [categoryRequest startAsynchronous];

}

-(void)getStores:(void (^)(NSArray *))completion
{
    //[self.mainMenuViewController setWaitInfoLabelText:@"Downloading Stores"];
    ASIFormDataRequest *storeRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *storeRequest_b = storeRequest;
    [storeRequest setPostValue:@"stores" forKey:@"object"];
    [storeRequest setPostValue:@"get_all" forKey:@"action"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [storeRequest setPostValue:[defaults objectForKey:@"kpid"] forKey:@"kpid"];
    [storeRequest setPostValue:vendorID forKey:@"vendor_id"];
    [storeRequest setFailedBlock:^{
        //[self.mainMenuViewController removeWaitView];
    }];
    [storeRequest setCompletionBlock:^{
        NSError* error;
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[storeRequest_b responseData] options:kNilOptions error:&error];
		//NSLog(@"stores: %@", dataArray);
		if (error != nil)
		{
			// ERROR
		}
        else
		{
			NSMutableArray *newStoreArray = [[NSMutableArray alloc] init];
			for (NSDictionary *storeDict in dataArray)
			{
				StoreObject *newStore = [[StoreObject alloc] init];
                [newStore setStoreID:[NSString stringWithFormat:@"%@", [storeDict objectForKey:@"storeID"]]];
                [newStore setStoreName:[storeDict objectForKey:@"storeName"]];
                [newStore setStorePhone:[NSString stringWithFormat:@"%@", [storeDict objectForKey:@"storePhone"]]];
                [newStore setStoreEmail:[storeDict objectForKey:@"storeEmail"]];
                [newStore setStoreWebsite:[storeDict objectForKey:@"storeWebsite"]];
                [newStore setStoreStreetAddress:[storeDict objectForKey:@"storeStreetAddress"]];
                [newStore setStoreSuburb:[storeDict objectForKey:@"storeSuburb"]];
                [newStore setStoreCity:[storeDict objectForKey:@"storeRegion"]];
                [newStore setStorePostcode:[NSString stringWithFormat:@"%@", [storeDict objectForKey:@"storePostcode"]]];
                [newStore setStorePincode:[NSString stringWithFormat:@"%@", [storeDict objectForKey:@"storePincode"]]];
                [newStore setStoreLatitude:[NSString stringWithFormat:@"%@", [storeDict objectForKey:@"storeLatitude"]]];
                [newStore setStoreLongitude:[NSString stringWithFormat:@"%@", [storeDict objectForKey:@"storeLongitude"]]];
				[newStoreArray addObject:newStore];
			}
			
			NSArray *returnArray = [NSArray arrayWithArray:newStoreArray];
			completion(returnArray);
		}
        
        
    }];
    [storeRequest startAsynchronous];
}


-(void)getCouponsForCategory:(int)categoryID withCompletion:(void (^)(NSArray *))completion
{
	ASIFormDataRequest *couponRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *couponRequest_b = couponRequest;
    [couponRequest setPostValue:@"coupons" forKey:@"object"];
    [couponRequest setPostValue:@"get_by_category" forKey:@"action"];
	[couponRequest setPostValue:[NSString stringWithFormat:@"%d", categoryID] forKey:@"category_id"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [couponRequest setPostValue:[defaults objectForKey:@"kpid"] forKey:@"kpid"];
    [couponRequest setPostValue:vendorID forKey:@"vendor_id"];
    [couponRequest setFailedBlock:^{
		NSLog(@"FAILED: %@", [couponRequest_b responseString]);
    }];
    [couponRequest setCompletionBlock:^{
		NSLog(@"COMPLETION: %@", [couponRequest_b responseString]);
        NSError* error;
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[couponRequest_b responseData] options:kNilOptions error:&error];
		if (error != nil)
		{
			// ERROR
			completion(nil);
		}
		else
		{
			completion(dataArray);
		}
        
    }];
    [couponRequest startAsynchronous];
}


-(void)getCoupons:(void (^)(NSArray *))completion
{
    ASIFormDataRequest *couponRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *couponRequest_b = couponRequest;
    [couponRequest setPostValue:@"coupons" forKey:@"object"];
    [couponRequest setPostValue:@"get_recent" forKey:@"action"];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [couponRequest setPostValue:[defaults objectForKey:@"kpid"] forKey:@"kpid"];
    [couponRequest setPostValue:vendorID forKey:@"vendor_id"];
    [couponRequest setFailedBlock:^{
        //[self.mainMenuViewController removeWaitView];
    }];
    [couponRequest setCompletionBlock:^{
        NSError* error;
        NSArray *dataArray = [NSJSONSerialization JSONObjectWithData:[couponRequest_b responseData] options:kNilOptions error:&error];
		if (error != nil)
		{
			// ERROR
			//NSLog(@"coupon request error: %@", [error localizedDescription]);
			completion(nil);
		}
		else
		{
			completion(dataArray);
		}
        
    }];
    [couponRequest startAsynchronous];
}

-(void)startImageDownloads
{
    //NSLog(@"startImageDownloads:");
    
    if (currentDownloadCount == 0)
    {
        int max = 5;
        if ([self.downloaderArray count] < 5)
        {
            max = (int)[self.downloaderArray count];
        }
        
        for (NSUInteger i = 0; i < max; i++)
        {
            currentDownloadCount++;
            NSLog(@"start download for image: %@", [self.downloaderArray objectAtIndex:i]);
            __block NSURL *_url = [self.downloaderArray objectAtIndex:i];
            
            
            [SDWebImageDownloader.sharedDownloader downloadImageWithURL:[self.downloaderArray objectAtIndex:i]
                                     options:SDWebImageDownloaderProgressiveDownload
                                    progress:^(NSInteger receivedSize, NSInteger expectedSize){
                                        //NSLog(@"receivedSize: %lu", (unsigned long)receivedSize);
                                    }
                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                       //NSLog(@"image download for %@ completed. finished = %d", _url, finished);
                                       if(finished == YES && error == nil)
                                       {
                                           NSLog(@"image download for %@ completed with no errors", _url);
                                           [self downloadedImageWithURL:_url withImage:image withData:data withError:error isFinished:finished];
                                       }
                                       else if (finished == YES && error != nil)
                                       {
                                           NSLog(@"image download for %@ completed with errors: %@", _url, error);
                                           if ([error code] == -1001)
                                           {
                                               [SDWebImageDownloader.sharedDownloader downloadImageWithURL:_url
                                                                        options:SDWebImageDownloaderProgressiveDownload
                                                                       progress:nil
                                                                      completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                                          [self downloadedImageWithURL:_url withImage:image withData:data withError:error isFinished:finished];
                                                                      }];
                                           }
                                           else if ([error code] == -1200)
                                           {
                                               [SDWebImageDownloader.sharedDownloader downloadImageWithURL:_url
                                                                        options:SDWebImageDownloaderProgressiveDownload
                                                                       progress:nil
                                                                      completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                                          [self downloadedImageWithURL:_url withImage:image withData:data withError:error isFinished:finished];
                                                                      }];
                                           }
                                           else
                                           {
                                               totalImages--;
                                           }
                                       }
                                   }];
        }
    }
}

-(void)downloadedImageWithURL:(NSURL *)url withImage:(UIImage *)image withData:(NSData *)data withError:(NSError *)error isFinished:(BOOL)finished
{
    NSArray *urlArray = [[NSString stringWithFormat:@"%@", url] componentsSeparatedByString:@"coupon_images/"];
    if ([urlArray count] == 2)
    {
		NSArray *array = [NSArray arrayWithArray:self.downloadingArray];
        if (![array containsObject:[urlArray lastObject]])
        {
            [self.downloadingArray addObject:[urlArray lastObject]];
            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *imageFilePath = [NSString stringWithFormat:@"%@/%@",docDir, [urlArray lastObject]];
            BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:imageFilePath];
            if (fileExists == NO)
            {
                NSData *imageData;
                if ([[urlArray lastObject] hasSuffix:@".jpeg"])
                {
                    imageData = UIImageJPEGRepresentation(image, 1.0);
                }
                else
                {
                    imageData = UIImagePNGRepresentation(image);
                }
                
                if (imageData != nil)
                {
                    BOOL saved = [imageData writeToFile:imageFilePath atomically:YES];
                    if (saved)
                    {
                        [TCAppDelegate addSkipBackupAttributeToPath:imageFilePath];
                        currentDownloadCount--;
                        totalDownloads++;
                        //[self.mainMenuViewController setWaitInfoLabelText:[NSString stringWithFormat:@"Downloaded Coupon %d of %d", totalDownloads, imageDownloads]];
                        [self.downloaderArray removeObject:url];
						NSLog(@"wrote image to file. currentDownloadCount = %d, downloaderArray count: %lu", currentDownloadCount, (unsigned long)[self.downloaderArray count]);
                        if (currentDownloadCount == 0 && [self.downloaderArray count] > 0)
                        {
							NSLog(@"startImageDownloads: after delay");
                            //[self performSelector:@selector(startImageDownloads) withObject:nil afterDelay:0.5];
							[self startImageDownloads];
                        }
                        else if (currentDownloadCount == 0 && [self.downloaderArray count] == 0)
                        {
                            //[self.mainMenuViewController removeWaitView];
                        }
                    }
                    else
                    {
                        [SDWebImageDownloader.sharedDownloader downloadImageWithURL:url
                                                 options:SDWebImageDownloaderProgressiveDownload
                                                progress:nil
                                               completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                                   [self downloadedImageWithURL:url withImage:image withData:data withError:error isFinished:finished];
                                               }];
                    }
                    totalImages--;
                }
                else
                {
                    [SDWebImageDownloader.sharedDownloader downloadImageWithURL:url
                                             options:SDWebImageDownloaderProgressiveDownload
                                            progress:nil
                                           completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                                               [self downloadedImageWithURL:url withImage:image withData:data withError:error isFinished:finished];
                                           }];
                }
            }
            else
            {
                currentDownloadCount--;
                totalDownloads++;
                //[self.mainMenuViewController setWaitInfoLabelText:[NSString stringWithFormat:@"Checked Coupon %d of %d", totalDownloads, imageDownloads]];
                [self.downloaderArray removeObject:url];
                if (currentDownloadCount == 0 && [self.downloaderArray count] > 0)
                {
                    [self performSelector:@selector(startImageDownloads) withObject:nil afterDelay:0];
                }
                else if (currentDownloadCount == 0 && [self.downloaderArray count] == 0)
                {
                    //[self.mainMenuViewController removeWaitView];
                }
            }
        }
    }
    
    if (totalImages <= 0)
    {
        //[self.mainMenuViewController removeWaitView];
    }
}

-(void)sendAnyVerifiedCoupons
{
    self.isSendingCoupons = YES;
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	NSData *data = [defaults objectForKey:@"verified_coupons"];
	NSArray *myArray = [NSKeyedUnarchiver unarchiveObjectWithData:data];
	__block NSMutableArray *verifiedCouponArray = [myArray mutableCopy];
	[defaults synchronize];
	NSLog(@"sendAnyVerifiedCoupons: %@", verifiedCouponArray);
	
	if ([verifiedCouponArray count] > 0)
	{
		NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
		NSMutableArray *newCouponArray = [[NSMutableArray alloc] init];
		for (CouponVerificationObject *verifiedCouponObject in verifiedCouponArray)
		{
			[newCouponArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[verifiedCouponObject couponID], @"coupon_id", [verifiedCouponObject storeCode], @"code", [NSString stringWithFormat:@"%@", [verifiedCouponObject timestamp]], @"timestamp",[verifiedCouponObject valueUsed], @"value_used",[verifiedCouponObject storeID], @"store_id", nil]];
		}
		
		NSMutableArray *sendCouponArray = [[NSMutableArray alloc] init];
		
		for (NSUInteger i = 0; i < [newCouponArray count]; i++)
		{
			for(NSString *aKey in [newCouponArray objectAtIndex:i])
			{
				[sendCouponArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:[[newCouponArray objectAtIndex:i] objectForKey:aKey],@"value",[NSString stringWithFormat:@"coupons[%lu][%@]", (unsigned long)i, aKey],@"key",nil]];
			}
		}
		//NSLog(@"send coupon array: %@", sendCouponArray);
		
		ASIFormDataRequest *redeemCouponRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
		__weak ASIFormDataRequest *redeemCouponRequest_b = redeemCouponRequest;
		[redeemCouponRequest addPostValue:@"coupons" forKey:@"object"];
		[redeemCouponRequest addPostValue:@"verify" forKey:@"action"];
		[redeemCouponRequest addPostValue:sendCouponArray forKey:@"coupons"];
		[redeemCouponRequest addPostValue:[defaults objectForKey:@"kpid"] forKey:@"kpid"];
		[redeemCouponRequest setCompletionBlock:^{
			
			NSLog(@"array before: %@", verifiedCouponArray);
			NSError *error;
			NSArray *responseArray = [NSJSONSerialization JSONObjectWithData:[redeemCouponRequest_b responseData] options:kNilOptions error:&error];
			NSLog(@"response: %@", [redeemCouponRequest_b responseString]);
			for (NSDictionary *couponDict in responseArray)
			{
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				[dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
				NSDate *date = [dateFormatter dateFromString:[couponDict objectForKey:@"timestamp"]];
				NSArray *array = [NSArray arrayWithArray:verifiedCouponArray];
				for (CouponVerificationObject *couponVO in array)
				{
					if ([[couponVO couponID] isEqualToString:[couponDict objectForKey:@"coupon_id"]] && [[dateFormatter stringFromDate:[couponVO timestamp]] isEqualToString:[dateFormatter stringFromDate:date]])
					{
						[verifiedCouponArray removeObject:couponVO];
						if ([couponVO isSingleUse] == YES)
						{
							[[NSNotificationCenter defaultCenter] postNotificationName:kDidVerifySingleUseCouponNotification object:nil userInfo:[NSDictionary dictionaryWithObject:couponVO forKey:@"couponVerificationObject"]];
						}
						break;
					}
				}
			}
			NSArray *array = [NSArray arrayWithArray:verifiedCouponArray];
			NSData *archivedata = [NSKeyedArchiver archivedDataWithRootObject:array];
			[defaults setObject:archivedata forKey:@"verified_coupons"];
			[defaults synchronize];
			
			NSLog(@"array after: %@", verifiedCouponArray);
			
			self.isSendingCoupons = NO;
		}];
		[redeemCouponRequest setFailedBlock:^{
			NSLog(@"redeemCouponRequest: failed: %@", [redeemCouponRequest_b error]);
			
			
			self.isSendingCoupons = NO;
		}];
		[redeemCouponRequest startAsynchronous];
	}
}

-(void)loginWithLastName:(NSString *)lastName withRegistration:(NSString *)registration withCompletionBlock:(void (^)(BOOL))completion
{
	ASIFormDataRequest *loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *loginRequest_b = loginRequest;
    [loginRequest setPostValue:@"users" forKey:@"object"];
    [loginRequest setPostValue:@"login_one" forKey:@"action"];
	[loginRequest setPostValue:lastName forKey:@"lastname"];
	[loginRequest setPostValue:registration forKey:@"identifier"];
    [loginRequest setPostValue:vendorID forKey:@"vendor_id"];
    [loginRequest setFailedBlock:^{
        NSLog(@"loginRequestFailed: %@", [loginRequest_b responseString]);
		completion(NO);
    }];
    [loginRequest setCompletionBlock:^{
		NSLog(@"loginRequestCompleted: %@", [loginRequest_b responseString]);
		if ([[loginRequest_b responseString] isEqualToString:@"ERROR"])
		{
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			[defaults setObject:lastName forKey:@"surname"];
			completion(NO);
		}
		else
		{
			
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			[defaults setObject:[loginRequest_b responseString] forKey:@"kpid"];
			[defaults setObject:lastName forKey:@"surname"];
			[defaults setObject:registration forKey:@"identifier"];
			[defaults setObject:@"R" forKey:@"identifierType"];
			[defaults synchronize];
			[self registerWithUbiquity];
			[self sendUbiquityEvent:kAppOpenedEvent withDescription:kAppOpenedEventDescription];
			completion(YES);
		}
    }];
    [loginRequest startAsynchronous];
}

-(void)loginWithLastName:(NSString *)lastName withRegistration:(NSString *)registration asynchronous:(BOOL)asynchronous withCompletionBlock:(void (^)(BOOL))completion
{
	ASIFormDataRequest *loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *loginRequest_b = loginRequest;
    [loginRequest setPostValue:@"users" forKey:@"object"];
    [loginRequest setPostValue:@"login_one" forKey:@"action"];
	[loginRequest setPostValue:lastName forKey:@"lastname"];
	[loginRequest setPostValue:registration forKey:@"identifier"];
    [loginRequest setPostValue:vendorID forKey:@"vendor_id"];
    [loginRequest setFailedBlock:^{
        NSLog(@"loginRequestFailed: %@", [loginRequest_b responseString]);
		completion(NO);
    }];
    [loginRequest setCompletionBlock:^{
		NSLog(@"loginRequestCompleted: %@", [loginRequest_b responseString]);
		if ([[loginRequest_b responseString] isEqualToString:@"ERROR"])
		{
			completion(NO);
		}
		else
		{
			
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			[defaults setObject:[loginRequest_b responseString] forKey:@"kpid"];
			[defaults setObject:lastName forKey:@"surname"];
			[defaults setObject:registration forKey:@"identifier"];
			[defaults setObject:@"R" forKey:@"identifierType"];
			[defaults synchronize];
			[self registerWithUbiquity];
			[self sendUbiquityEvent:kAppOpenedEvent withDescription:kAppOpenedEventDescription];
			completion(YES);
		}
    }];
	
	if (asynchronous == YES)
	{
		[loginRequest startAsynchronous];
	}
	else
	{
		[loginRequest startSynchronous];
	}
    
}

-(void)loginThreeWithLastName:(NSString *)lastName withRegistration:(NSString *)registration withCompletionBlock:(void (^)(BOOL))completion
{
	ASIFormDataRequest *loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *loginRequest_b = loginRequest;
    [loginRequest setPostValue:@"users" forKey:@"object"];
    [loginRequest setPostValue:@"login_three" forKey:@"action"];
	[loginRequest setPostValue:lastName forKey:@"lastname"];
	[loginRequest setPostValue:registration forKey:@"identifier"];
    [loginRequest setPostValue:vendorID forKey:@"vendor_id"];
    [loginRequest setFailedBlock:^{
        NSLog(@"loginRequestFailed: %@", [loginRequest_b responseString]);
		completion(NO);
    }];
    [loginRequest setCompletionBlock:^{
		NSLog(@"loginRequestCompleted: %@", [loginRequest_b responseString]);
		if ([[loginRequest_b responseString] isEqualToString:@"ERROR"])
		{
			completion(NO);
		}
		else
		{
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			[defaults setObject:[loginRequest_b responseString] forKey:@"kpid"];
			[defaults setObject:nil forKey:@"surname"];
			[defaults setObject:nil forKey:@"identifier"];
			[defaults setObject:nil forKey:@"identifierType"];
			[defaults synchronize];
			[self registerWithUbiquity];
			[self sendUbiquityEvent:kAppOpenedEvent withDescription:kAppOpenedEventDescription];
			completion(YES);
		}
    }];
    [loginRequest startAsynchronous];
}

-(void)loginWithPolicyNumber:(NSString *)policyNumber withCompletionBlock:(void (^)(BOOL))completion
{
	ASIFormDataRequest *loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *loginRequest_b = loginRequest;
    [loginRequest setPostValue:@"users" forKey:@"object"];
    [loginRequest setPostValue:@"login_two" forKey:@"action"];
	[loginRequest setPostValue:policyNumber forKey:@"policy_number"];
    [loginRequest setPostValue:vendorID forKey:@"vendor_id"];
    [loginRequest setFailedBlock:^{
        NSLog(@"loginRequestFailed: %@", [loginRequest_b responseString]);
		completion(NO);
    }];
    [loginRequest setCompletionBlock:^{
		NSLog(@"loginRequestCompleted: %@", [loginRequest_b responseString]);
		if ([[loginRequest_b responseString] isEqualToString:@"ERROR"])
		{
			completion(NO);
		}
		else
		{
			NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
			[defaults setObject:[loginRequest_b responseString] forKey:@"kpid"];
			[defaults setObject:policyNumber forKey:@"identifier"];
			[defaults setObject:@"P" forKey:@"identifierType"];
			completion(YES);
		}
    }];
    [loginRequest startAsynchronous];
}

//registerdevice/{deviceID}/{deviceType}/{pushToken}?authToken={authToken}&surname={surname}&identifierType={identifierType}&identifier={identifier}
-(void)registerWithUbiquity
{
	NSString *pushTokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"kNotificationToken"];
	if ([[NSUserDefaults standardUserDefaults] objectForKey:@"surname"] != nil)
	{
		ASIHTTPRequest *registrationRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:ubiqityFullURL, [[NSUserDefaults standardUserDefaults] objectForKey:@"kpid"], pushTokenString, kAuthToken, [[NSUserDefaults standardUserDefaults] objectForKey:@"surname"], [[NSUserDefaults standardUserDefaults] objectForKey:@"identifierType"], [[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]]]];
		__block ASIHTTPRequest *registrationRequest_b = registrationRequest;
		[registrationRequest setFailedBlock:^{
			NSLog(@"registerWithUbiquityFailed: %@", [registrationRequest_b error]);
		}];
		[registrationRequest setCompletionBlock:^{
			NSLog(@"registerWithUbiquityCompleted: %@", [registrationRequest_b responseString]);
		}];
		[registrationRequest startAsynchronous];
	}
	else
	{
		ASIHTTPRequest *registrationRequest = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:ubiqityShortURL, [[NSUserDefaults standardUserDefaults] objectForKey:@"kpid"], pushTokenString, kAuthToken]]];
		__block ASIHTTPRequest *registrationRequest_b = registrationRequest;
		[registrationRequest setFailedBlock:^{
			NSLog(@"registerWithUbiquityFailed: %@", [registrationRequest_b error]);
		}];
		[registrationRequest setCompletionBlock:^{
			NSLog(@"registerWithUbiquityCompleted: %@", [registrationRequest_b responseString]);
			
		}];
		[registrationRequest startAsynchronous];
	}
	
}

// URL EXTENSTION AFTER SHORT URL
//  appendevent?authToken={authToken}&surname={surname}&identifierType={identifierType}&identifier={identifier}

// JSON DATA FORMAT FOR SENDING EVENT INFO
/*
{
	"EventDateTime": "2014-11-17T15:59:01.5136664+13:00",
	"EventType": "sample string 2",
	"Description": "sample string 3",
	"DeviceType": "sample string 4"
}

*/

static NSString *dateFormat = @"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ";
static NSString *jsonString = @"{\"EventDateTime\":\"%@\",\"EventType\":\"%@\",\"Description\":\"%@\",\"DeviceType\":\"IOS\"}";

//static NSString *ubiqityEventURL = @"https://amiassist-staging.uq.co.nz/appendevent?authToken=%@&surname=%@&identifierType=%@&identifier=%@";
-(void)sendUbiquityEvent:(NSString *)eventType withDescription:(NSString *)description
{
	//NSString *pushTokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"kNotificationToken"];
	 ASIFormDataRequest *eventRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:ubiqityEventURL, kAuthToken, [[NSUserDefaults standardUserDefaults] objectForKey:@"surname"], [[NSUserDefaults standardUserDefaults] objectForKey:@"identifierType"], [[NSUserDefaults standardUserDefaults] objectForKey:@"identifier"]]]];
	__weak ASIFormDataRequest *eventRequest_b = eventRequest;
	
	[eventRequest addRequestHeader:@"Content-Type" value:@"application/json"];
	
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:dateFormat];
	[dateFormatter setTimeZone:[NSTimeZone defaultTimeZone]];
	
	[eventRequest appendPostData:[[NSString stringWithFormat:jsonString,[dateFormatter stringFromDate:[NSDate date]],eventType, description] dataUsingEncoding:NSUTF8StringEncoding]];
	[eventRequest setFailedBlock:^{
		NSLog(@"EVENT REQUEST FAILED: %@", [eventRequest_b error]);
	}];
	[eventRequest setCompletionBlock:^{
		NSLog(@"EVENT REQUEST COMPLETED: %d, %@", [eventRequest_b responseStatusCode], [eventRequest_b responseStatusMessage]);
	}];
	[eventRequest startAsynchronous];
	
	
}

-(void)loadBranchDataFromCSVwithCompletion:(void (^)(NSArray *))completion
{
    NSString *infoSouceFile = [[NSBundle mainBundle] pathForResource:@"AMI_Branches" ofType:@"csv"];
    NSString *fileContents = [NSString stringWithContentsOfFile:infoSouceFile encoding:NSUTF8StringEncoding error:nil];
    
    NSArray *linesOfText = [fileContents componentsSeparatedByString:@"\n"];
	
	NSMutableArray *branchArray = [[NSMutableArray alloc] init];
	
    for (NSString *line in linesOfText)
    {
        NSArray *items = [line componentsSeparatedByString:@";"];
        if ([items count] == 9)
        {
			CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[items objectAtIndex:7] doubleValue], [[items objectAtIndex:8] doubleValue]);
            AMIBranchObject *amiBranch = [[AMIBranchObject alloc] initWithCoordinate:coordinate];
			[amiBranch setBranchName:[NSString stringWithFormat:@"%@ Branch",[items objectAtIndex:0]]];
			[amiBranch setBranchPhone:[items objectAtIndex:1]];
			[amiBranch setBranchEmail:[items objectAtIndex:2]];
			[amiBranch setBranchStreet:[items objectAtIndex:3]];
			if ([[items objectAtIndex:4] isEqualToString:@"-"])
			{
				[amiBranch setBranchSuburb:@""];
			}
			else
			{
				[amiBranch setBranchSuburb:[items objectAtIndex:4]];
			}
			[amiBranch setBranchRegion:[items objectAtIndex:5]];
			[amiBranch setBranchPostcode:[items objectAtIndex:6]];
            [branchArray addObject:amiBranch];
        }
		else
		{
			NSLog(@"excluding: %@ with %lu items", items, (unsigned long)items.count);
		}
    }
	
	completion(branchArray);
}

-(void)validateKPIDwithCompletionBlock:(void (^)(BOOL))completion
{
	ASIFormDataRequest *loginRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:baseURL]];
    __weak ASIFormDataRequest *loginRequest_b = loginRequest;
    [loginRequest setPostValue:@"users" forKey:@"object"];
    [loginRequest setPostValue:@"validate_kpid" forKey:@"action"];
	[loginRequest addPostValue:[[NSUserDefaults standardUserDefaults] objectForKey:@"kpid"] forKey:@"kpid"];
    [loginRequest setPostValue:vendorID forKey:@"vendor_id"];
    [loginRequest setFailedBlock:^{
        NSLog(@"loginRequestFailed: %@", [loginRequest_b responseString]);
		completion(NO);
    }];
    [loginRequest setCompletionBlock:^{
		if ([[loginRequest_b responseString] isEqualToString:@"ERROR"])
		{
			completion(NO);
		}
		else
		{
			if ([[loginRequest_b responseString] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"kpid"]])
			{
				[self sendUbiquityEvent:kAppOpenedEvent withDescription:kAppOpenedEventDescription];
				completion(YES);
			}
			else
			{
				completion(NO);
			}
		}
    }];
    [loginRequest startSynchronous];
}


+ (void)addSkipBackupAttributeToPath:(NSString*)path {
    u_int8_t b = 1;
    setxattr([path fileSystemRepresentation], "com.apple.MobileBackup", &b, 1, 0, 0);
}


#pragma mark - UALocationServiceDelegate

/**
 * Updates the delegate when the location service generates an error.
 *
 * @warning Two error conditions will stop the location service before
 * this method is called on the delegate. kCLErrorDenied && kCLErrorNetwork. All other
 * CoreLocation errors are passed directly to the delegate without any side effects. For the case
 * when the location service times out, an NSError* UALocationServiceError will be returned with
 * the best available location in the userInfo dictionary with UALocationServiceBestAvailableSingleLocationKey
 * as the key if a location was returned. In the case of a timeout, the locationService:didUpdateLocations: will also
 * be called with the best available location if it is available. There is no guarantee as to the accuracy of this location.
 *
 * @param service Location service that generated the error
 * @param error Error passed from a CLLocationManager
 */
- (void)locationService:(UALocationService *)service didFailWithError:(NSError *)error
{
	
}

/**
 * Updates the delegate when authorization status has changed.
 *
 * @param service Location service reporting the change
 * @param status  The updated location authorization status
 */
- (void)locationService:(UALocationService *)service didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
	NSLog(@"UALocationService: didChangeAuthorizationStatus: %d", status);
	
	if (status == kCLAuthorizationStatusAuthorized)
	{
		UALocationService *uaLocationService = [[UAirship shared] locationService];
		uaLocationService.backgroundLocationServiceEnabled = YES;
		[uaLocationService startReportingSignificantLocationChanges];
	}
}

/**
 * Updates the delegate when a new location is received. In case of a timeout, where locations have been acquired,
 * but they do not meet the accuracy requirements, the most accurate location available will be returned.
 *
 * @warning In the background, this method is called and given a limited amount of time to operate, including the time
 * necessary to update UrbanAirship. Extensive work done by the method while backgrounded could result in location data not being
 * recorded or sent.
 *
 * @param service The service reporting the location update
 * @param locations The updated locations reported by the service
 */
- (void)locationService:(UALocationService *)service didUpdateLocations:(NSArray *)locations
{
	NSLog(@"UALocationService:   didUpdateLocations: %@", locations);
	[[[UAirship shared] locationService] reportCurrentLocation];
}

@end
