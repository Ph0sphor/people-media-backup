//
//  TCHelpViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 1/07/13.
//  Copyright (c) 2013 Judson Steel. All rights reserved.
//

#import "TCHelpViewController.h"

@interface TCHelpViewController ()

@end

@implementation TCHelpViewController

-(IBAction)back:(id)sender
{
	[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)callFreephone:(id)sender
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://0508222411"]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [scrollView setContentSize:CGSizeMake(contentView.frame.size.width, contentView.frame.size.height)];
	
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"How to Use"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
