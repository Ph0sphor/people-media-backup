//
//  TCOffersViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCOffersViewController.h"
#import "TCCouponMapSummaryViewController.h"
#import "TCCouponCategorySummaryViewController.h"
#import "TCCouponContainerViewController.h"
#import "TCCouponSummaryViewController.h"
#import "TCCategoryTableViewCell.h"

@interface TCOffersViewController ()

@end

@implementation TCOffersViewController

-(IBAction)back:(id)sender
{
	[[self navigationController] popViewControllerAnimated:YES];
}

-(void)setCategoryArray:(NSArray *)categories
{
	categoryArray = categories;
	[offersTableView reloadData];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Offer List"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showNearby"])
	{
		TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
		 TCCouponMapSummaryViewController *mapVC = segue.destinationViewController;
		[mapVC setCategoryArray:categoryArray];
		[mapVC setStoreArray:self.storesArray];
		[appDelegate getCoupons:^(NSArray *coupons) {
			NSMutableArray *newCouponArray = [[NSMutableArray alloc] init];
			for (NSDictionary *couponDict in coupons)
			{
				CouponObject *newCoupon = [[CouponObject alloc] init];
				[newCoupon setCouponID:[[couponDict objectForKey:@"id"] intValue]];
				[newCoupon setCategoryID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"category_id"]]];
				
				for (CategoryObject *categoryObject in categoryArray)
				{
					if ([categoryObject categoryID] == [newCoupon.categoryID intValue])
					{
						[newCoupon setCategoryColor:[categoryObject categoryColor]];
						break;
					}
				}
				
				[newCoupon setCouponTypeID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"type_id"]]];
				[newCoupon setCouponDescription:[couponDict objectForKey:@"description"]];
				[newCoupon setCouponTypeDescription:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"type_description"]]];
				[newCoupon setCouponValue:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"value"]]];
				[newCoupon setCouponExtraInfo:[couponDict objectForKey:@"extra_info"]];
				[newCoupon setLoyaltyRewardCouponID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"loyalty_reward_coupon_id"]]];
				[newCoupon setLoyaltyThreshold:[NSNumber numberWithInt:[[couponDict objectForKey:@"loyalty_threshold"] intValue]]];
				[newCoupon setCouponParentID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"parent_id"]]];
				[newCoupon setCouponParentType:[couponDict objectForKey:@"parent_type"]];
				if ([[couponDict objectForKey:@"redeemable"] isEqualToString:@"YES"])
				{
					[newCoupon setCouponIsRedeemable:[NSNumber numberWithBool:YES]];
				}
				else
				{
					[newCoupon setCouponIsRedeemable:[NSNumber numberWithBool:NO]];
				}
				
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				[dateFormatter setDateFormat:@"yyyy-MM-dd"];
				
				
				if ([[couponDict objectForKey:@"repeat"] isEqualToString:@"YES"])
				{
					[newCoupon setCouponIsRepeat:[NSNumber numberWithBool:YES]];
					[dateFormatter setDateFormat:@"yyyy-MM-dd"];
					[newCoupon setCouponRepeatStartDate:[dateFormatter dateFromString:[couponDict objectForKey:@"repeat_start"]]];
					[newCoupon setCouponRepeatEndDate:[dateFormatter dateFromString:[couponDict objectForKey:@"repeat_end"]]];
				}
				else
				{
					[newCoupon setCouponIsRepeat:[NSNumber numberWithBool:NO]];
				}
				[newCoupon setCouponTermsConditions:[couponDict objectForKey:@"terms_conditions"]];
				[newCoupon setCouponTitle:[couponDict objectForKey:@"title"]];
				
				if (![[couponDict objectForKey:@"image_url"] isEqualToString:@""])
				{
					[newCoupon setCouponImageURL:[couponDict objectForKey:@"image_url"]];
				}
				else
				{
					[newCoupon setCouponImageURL:@"Default"];
				}
				
				if (![[couponDict objectForKey:@"image_url_2x"] isEqualToString:@""])
				{
					[newCoupon setCouponImageURLRetina:[couponDict objectForKey:@"image_url_2x"]];
				}
				else
				{
					[newCoupon setCouponImageURLRetina:@"Default"];
				}
				
				NSMutableArray *newStoreArray = [[NSMutableArray alloc] init];
				
				NSString *storeString = [couponDict objectForKey:@"store_ids"];
				NSArray *storeStringArray = [storeString componentsSeparatedByString:@","];
				for (NSUInteger i = 0; i < [storeStringArray count]; i++)
				{
					//NSLog(@"searching for store: %@", [storeStringArray objectAtIndex:i]);
					for (StoreObject *store in self.storesArray)
					{
						if ([[store storeID] isEqualToString:[storeStringArray objectAtIndex:i]])
						{
							[newStoreArray addObject:store];
							break;
						}
					}
				}
				
				if ([CLLocationManager locationServicesEnabled])
				{
					CLLocation *currentLocation = [appDelegate getCurrentLocation];
					if (currentLocation != nil && newStoreArray.count > 1)
					{
						NSArray *sortedStoreArray = [newStoreArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
							
							CLLocation *locationA = [[CLLocation alloc] initWithLatitude:[[(StoreObject *)obj1 storeLatitude] doubleValue] longitude:[[(StoreObject *)obj1 storeLongitude] doubleValue]];
							CLLocation *locationB = [[CLLocation alloc] initWithLatitude:[[(StoreObject *)obj2 storeLatitude] doubleValue] longitude:[[(StoreObject *)obj2 storeLongitude] doubleValue]];
							CLLocationDistance distanceA = [locationA distanceFromLocation:currentLocation];
							CLLocationDistance distanceB = [locationB distanceFromLocation:currentLocation];
							
							if (distanceA < distanceB)
							{
								return NSOrderedAscending;
							}
							else if (distanceA > distanceB)
							{
								return NSOrderedDescending;
							}
							else
							{
								return NSOrderedSame;
							}
							
						}];
						[newCoupon setStoreArray:sortedStoreArray];
					}
					else
					{
						[newCoupon setStoreArray:newStoreArray];
					}
				}
				else
				{
					[newCoupon setStoreArray:newStoreArray];
				}
				
				[newCouponArray addObject:newCoupon];
			}

			[mapVC setCouponArray:newCouponArray];
		}];
	}
	else if ([segue.identifier isEqualToString:@"showCouponSummary"])
	{
		TCCouponSummaryViewController *categoryVC = segue.destinationViewController;
		[categoryVC setCategory:self.tempCategory];
		
		TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate getCouponsForCategory:self.tempCategory.categoryID withCompletion:^(NSArray *coupons) {
			NSLog(@"gotCoupons: category = %d\ncoupons: %@", self.tempCategory.categoryID, coupons);
			
			NSMutableArray *couponArray = [[NSMutableArray alloc] init];
			for (NSDictionary *couponDict in coupons)
			{
				CouponObject *newCoupon = [[CouponObject alloc] init];
				[newCoupon setCouponID:[[couponDict objectForKey:@"id"] intValue]];
				[newCoupon setCategoryID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"category_id"]]];
				
				for (CategoryObject *categoryObject in categoryArray)
				{
					if ([categoryObject categoryID] == [newCoupon.categoryID intValue])
					{
						[newCoupon setCategoryColor:[categoryObject categoryColor]];
						break;
					}
				}
				
				
				[newCoupon setCouponTypeID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"type_id"]]];
				[newCoupon setCouponDescription:[couponDict objectForKey:@"description"]];
				[newCoupon setCouponTypeDescription:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"type_description"]]];
				[newCoupon setCouponValue:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"value"]]];
				[newCoupon setCouponExtraInfo:[couponDict objectForKey:@"extra_info"]];
				[newCoupon setLoyaltyRewardCouponID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"loyalty_reward_coupon_id"]]];
				[newCoupon setLoyaltyThreshold:[NSNumber numberWithInt:[[couponDict objectForKey:@"loyalty_threshold"] intValue]]];
				[newCoupon setCouponParentID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"parent_id"]]];
				[newCoupon setCouponParentType:[couponDict objectForKey:@"parent_type"]];
				if ([[couponDict objectForKey:@"redeemable"] isEqualToString:@"YES"])
				{
					[newCoupon setCouponIsRedeemable:[NSNumber numberWithBool:YES]];
				}
				else
				{
					[newCoupon setCouponIsRedeemable:[NSNumber numberWithBool:NO]];
				}
				
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				[dateFormatter setDateFormat:@"yyyy-MM-dd"];
				
				
				if ([[couponDict objectForKey:@"repeat"] isEqualToString:@"YES"])
				{
					[newCoupon setCouponIsRepeat:[NSNumber numberWithBool:YES]];
					[dateFormatter setDateFormat:@"yyyy-MM-dd"];
					[newCoupon setCouponRepeatStartDate:[dateFormatter dateFromString:[couponDict objectForKey:@"repeat_start"]]];
					[newCoupon setCouponRepeatEndDate:[dateFormatter dateFromString:[couponDict objectForKey:@"repeat_end"]]];
				}
				else
				{
					[newCoupon setCouponIsRepeat:[NSNumber numberWithBool:NO]];
				}
				[newCoupon setCouponTermsConditions:[couponDict objectForKey:@"terms_conditions"]];
				[newCoupon setCouponTitle:[couponDict objectForKey:@"title"]];
				
				if (![[couponDict objectForKey:@"image_url"] isEqualToString:@""])
				{
					[newCoupon setCouponImageURL:[couponDict objectForKey:@"image_url"]];
				}
				else
				{
					[newCoupon setCouponImageURL:@"Default"];
				}
				
				if (![[couponDict objectForKey:@"image_url_2x"] isEqualToString:@""])
				{
					[newCoupon setCouponImageURLRetina:[couponDict objectForKey:@"image_url_2x"]];
				}
				else
				{
					[newCoupon setCouponImageURLRetina:@"Default"];
				}
				
				NSMutableArray *newStoreArray = [[NSMutableArray alloc] init];
				
				NSString *storeString = [couponDict objectForKey:@"store_ids"];
				NSArray *storeStringArray = [storeString componentsSeparatedByString:@","];
				for (NSUInteger i = 0; i < [storeStringArray count]; i++)
				{
					//NSLog(@"searching for store: %@", [storeStringArray objectAtIndex:i]);
					for (StoreObject *store in self.storesArray)
					{
						if ([[store storeID] isEqualToString:[storeStringArray objectAtIndex:i]])
						{
							[newStoreArray addObject:store];
							break;
						}
					}
				}
				
				if ([CLLocationManager locationServicesEnabled])
				{
					CLLocation *currentLocation = [appDelegate getCurrentLocation];
					if (currentLocation != nil && newStoreArray.count > 1)
					{
						NSArray *sortedStoreArray = [newStoreArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
							
							CLLocation *locationA = [[CLLocation alloc] initWithLatitude:[[(StoreObject *)obj1 storeLatitude] doubleValue] longitude:[[(StoreObject *)obj1 storeLongitude] doubleValue]];
							CLLocation *locationB = [[CLLocation alloc] initWithLatitude:[[(StoreObject *)obj2 storeLatitude] doubleValue] longitude:[[(StoreObject *)obj2 storeLongitude] doubleValue]];
							CLLocationDistance distanceA = [locationA distanceFromLocation:currentLocation];
							CLLocationDistance distanceB = [locationB distanceFromLocation:currentLocation];
							
							if (distanceA < distanceB)
							{
								return NSOrderedAscending;
							}
							else if (distanceA > distanceB)
							{
								return NSOrderedDescending;
							}
							else
							{
								return NSOrderedSame;
							}
							
						}];
						[newCoupon setStoreArray:sortedStoreArray];
					}
					else
					{
						[newCoupon setStoreArray:newStoreArray];
					}
				}
				else
				{
					[newCoupon setStoreArray:newStoreArray];
				}
				
				
				[couponArray addObject:newCoupon];
			}
			[categoryVC setCouponArray:couponArray];
			[categoryVC reloadData];
			self.tempCategory = nil;
		}];
	}
}

-(void)dealloc
{
	self.storesArray = nil;
}


#pragma mark - UITableView Data and Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return ([categoryArray count] + 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		static NSString *CellIdentifier = @"couponCellIdentifier";
		TCCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		[cell.titleLabel setText:@"Nearby"];
		[cell.bgImage setImage:[UIImage imageNamed:@"light-yellow-1.png"]];
		return cell;
	}
	else
	{
		static NSString *CellIdentifier = @"couponCellIdentifier";
		TCCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
		cell.myCategory = [categoryArray objectAtIndex:(indexPath.row - 1)];
		
		[cell.titleLabel setText:[[categoryArray objectAtIndex:(indexPath.row - 1)] getCategoryTitle]];
		if (indexPath.row % 2 == 0)
		{
			[cell.bgImage setImage:[UIImage imageNamed:@"light-yellow-1.png"]];
		}
		else
		{
			[cell.bgImage setImage:[UIImage imageNamed:@"light-yellow-2.png"]];
		}
		return cell;
	}
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.row == 0)
	{
		[self performSegueWithIdentifier:@"showNearby" sender:self];
	}
	else if ((indexPath.row - 1) < categoryArray.count)
	{
		self.tempCategory = [categoryArray objectAtIndex:(indexPath.row - 1)];
		id tracker = [[GAI sharedInstance] defaultTracker];
		[tracker set:kGAIScreenName value:[NSString stringWithFormat:@"Offer - %@", [(CategoryObject *)[categoryArray objectAtIndex:(indexPath.row - 1)] getCategoryTitle]]];
		[tracker send:[[GAIDictionaryBuilder createAppView] build]];
		NSLog(@"showCategory: id: %d, name: %@", [(CategoryObject *)[categoryArray objectAtIndex:(indexPath.row - 1)] categoryID], [(CategoryObject *)[categoryArray objectAtIndex:(indexPath.row - 1)] getCategoryTitle]);
		[self performSegueWithIdentifier:@"showCouponSummary" sender:self];
	}
}

@end
