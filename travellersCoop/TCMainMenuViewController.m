//
//  TCMainMenuViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCMainMenuViewController.h"
#import "TCCouponMapSummaryViewController.h"
#import "TCCouponCategorySummaryViewController.h"
#import "TCOffersViewController.h"
#import "TCBranchFinderViewController.h"
#import "CategoryObject.h"

@interface TCMainMenuViewController ()

@end

@implementation TCMainMenuViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	NSLog(@"TCMainMenuViewController: viewDidLoad");
	
	[self getCategories];
	[self getStores];
	
	if (self.branchArray == nil)
	{
		TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate loadBranchDataFromCSVwithCompletion:^(NSArray *branches) {
			self.branchArray = branches;
		}];
	}
	
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Home Screen"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

-(void)getCategories
{
	NSLog(@"getCategories");
	TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate getCategories:^(NSArray *categories) {
		if (categories != nil)
		{
			[self setCategoryArray:categories];
			if (self.storesArray != nil && self.storesArray.count > 0)
			{
				NSLog(@"ENABLE BUTTON from get categories call");
				[offersButton setEnabled:YES];
				[loadingLabel setHidden:YES];
			}
			else
			{
				NSLog(@"NO STORES YET");
			}
		}
		else
		{
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Server Connection Error"
																message:@"Something went wrong while trying to contact the server. You can retry now, or come back again in a few minutes."
															   delegate:self
													  cancelButtonTitle:@"OK"
													  otherButtonTitles:@"Retry", nil];
			[alertView setTag:1];
			[alertView show];
		}
	}];
	
}

-(void)getStores
{
	NSLog(@"getStores");
	TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
	[appDelegate getStores:^(NSArray *stores) {
		if (stores != nil)
		{
			[self setStoresArray:stores];
			if (self.categoryArray != nil && self.categoryArray.count > 0)
			{
				NSLog(@"ENABLE BUTTON from get stores call");
				[offersButton setEnabled:YES];
				[loadingLabel setHidden:YES];
			}
			else
			{
				NSLog(@"NO CATEGORIES YET");
			}
		}
		else
		{
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Server Connection Error"
																message:@"Something went wrong while trying to contact the server. You can retry now, or come back again in a few minutes."
															   delegate:self
													  cancelButtonTitle:@"OK"
													  otherButtonTitles:@"Retry", nil];
			[alertView setTag:2];
			[alertView show];
		}
	}];
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	if (buttonIndex != alertView.cancelButtonIndex)
	{
		if (alertView.tag == 1)
		{
			[self getCategories];
		}
		else if (alertView.tag == 2)
		{
			[self getStores];
		}
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
	NSLog(@"Main menu: dealloc");
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"showNearby"])
	{
		TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
		__weak TCCouponMapSummaryViewController *mapVC = segue.destinationViewController;
		[mapVC setStoreArray:self.storesArray];
		[appDelegate getCoupons:^(NSArray *coupons) {
			NSMutableArray *couponArray = [[NSMutableArray alloc] init];
			//NSLog(@"couponArray: %@", couponArray);
			for (NSDictionary *couponDict in coupons)
			{
				CouponObject *newCoupon = [[CouponObject alloc] init];
				[newCoupon setCouponID:[[couponDict objectForKey:@"id"] intValue]];
				[newCoupon setCategoryID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"category_id"]]];
				
				for (CategoryObject *categoryObject in self.categoryArray)
				{
					if ([categoryObject categoryID] == [newCoupon.categoryID intValue])
					{
						[newCoupon setCategoryColor:[categoryObject categoryColor]];
						break;
					}
				}
				
				
				[newCoupon setCouponTypeID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"type_id"]]];
				[newCoupon setCouponDescription:[couponDict objectForKey:@"description"]];
				[newCoupon setCouponTypeDescription:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"type_description"]]];
				[newCoupon setCouponValue:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"value"]]];
				[newCoupon setCouponExtraInfo:[couponDict objectForKey:@"extra_info"]];
				[newCoupon setLoyaltyRewardCouponID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"loyalty_reward_coupon_id"]]];
				[newCoupon setLoyaltyThreshold:[NSNumber numberWithInt:[[couponDict objectForKey:@"loyalty_threshold"] intValue]]];
				[newCoupon setCouponParentID:[NSString stringWithFormat:@"%@", [couponDict objectForKey:@"parent_id"]]];
				[newCoupon setCouponParentType:[couponDict objectForKey:@"parent_type"]];
				if ([[couponDict objectForKey:@"redeemable"] isEqualToString:@"YES"])
				{
					[newCoupon setCouponIsRedeemable:[NSNumber numberWithBool:YES]];
				}
				else
				{
					[newCoupon setCouponIsRedeemable:[NSNumber numberWithBool:NO]];
				}
				
				NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
				[dateFormatter setDateFormat:@"yyyy-MM-dd"];
				
				
				if ([[couponDict objectForKey:@"repeat"] isEqualToString:@"YES"])
				{
					[newCoupon setCouponIsRepeat:[NSNumber numberWithBool:YES]];
					[dateFormatter setDateFormat:@"yyyy-MM-dd"];
					[newCoupon setCouponRepeatStartDate:[dateFormatter dateFromString:[couponDict objectForKey:@"repeat_start"]]];
					[newCoupon setCouponRepeatEndDate:[dateFormatter dateFromString:[couponDict objectForKey:@"repeat_end"]]];
				}
				else
				{
					[newCoupon setCouponIsRepeat:[NSNumber numberWithBool:NO]];
				}
				[newCoupon setCouponTermsConditions:[couponDict objectForKey:@"terms_conditions"]];
				[newCoupon setCouponTitle:[couponDict objectForKey:@"title"]];
				
				if (![[couponDict objectForKey:@"image_url"] isEqualToString:@""])
				{
					[newCoupon setCouponImageURL:[couponDict objectForKey:@"image_url"]];
				}
				else
				{
					[newCoupon setCouponImageURL:@"Default"];
				}
				
				if (![[couponDict objectForKey:@"image_url_2x"] isEqualToString:@""])
				{
					[newCoupon setCouponImageURLRetina:[couponDict objectForKey:@"image_url_2x"]];
				}
				else
				{
					[newCoupon setCouponImageURLRetina:@"Default"];
				}
				
				NSMutableArray *newStoreArray = [[NSMutableArray alloc] init];
				
				NSString *storeString = [couponDict objectForKey:@"store_ids"];
				NSArray *storeStringArray = [storeString componentsSeparatedByString:@","];
				for (NSUInteger i = 0; i < [storeStringArray count]; i++)
				{
					//NSLog(@"searching for store: %@", [storeStringArray objectAtIndex:i]);
					for (StoreObject *store in self.storesArray)
					{
						if ([[store storeID] isEqualToString:[storeStringArray objectAtIndex:i]])
						{
							[newStoreArray addObject:store];
							break;
						}
					}
				}
				[newCoupon setStoreArray:newStoreArray];
			}
			NSLog(@"couponArray: %@", couponArray);
			[mapVC setCouponArray:couponArray];
		}];
	}
	else if ([segue.identifier isEqualToString:@"showOffers"])
	{
		TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
		__weak TCOffersViewController *offersVC = segue.destinationViewController;
		[offersVC setStoresArray:self.storesArray];
		[offersVC setCategoryArray:self.categoryArray];
		[appDelegate getCategories:^(NSArray *categories) {
			if (categories != nil)
			{
				[offersVC setCategoryArray:categories];
			}
		}];
	}
	else if ([segue.identifier isEqualToString:@"showBranchFinder"])
	{
		TCBranchFinderViewController *branchFinderVC = segue.destinationViewController;
		[branchFinderVC setBranchArray:self.branchArray];
	}
}

-(IBAction)callAMI:(id)sender
{
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://0800505511"]];
}
@end
