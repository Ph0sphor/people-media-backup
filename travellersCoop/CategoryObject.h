//
//  CategoryObject.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 31/07/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoryObject : NSObject
@property (nonatomic, assign) int categoryID;
@property (nonatomic, strong) NSString *categoryName;
@property (nonatomic, strong) UIColor *categoryColor;
@property (nonatomic, assign) int parentID;
@property (nonatomic, assign) int vendorID;
-(NSString *)getCategoryTitle;
@end
