//
//  TCMainMenuViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCAppDelegate.h"
#import "CouponObject.h"

@interface TCMainMenuViewController : UIViewController <UIAlertViewDelegate>
{
	IBOutlet UIButton *offersButton;
	IBOutlet UILabel *loadingLabel;

}
@property (nonatomic, strong) NSArray *categoryArray;
@property (nonatomic, strong) NSArray *storesArray;
@property (nonatomic, strong) NSArray *branchArray;
- (IBAction)callAMI:(id)sender;
@end
