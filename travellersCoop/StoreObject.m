//
//  StoreObject.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 18/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "StoreObject.h"
#import "CouponObject.h"


@implementation StoreObject

-(id)init
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"name: %@, phone: %@, email: %@, website: %@, streetAddress: %@, suburb: %@, region: %@, postcode: %@, lat: %@, lng: %@", self.storeName, self.storePhone, self.storeEmail, self.storeWebsite, self.storeStreetAddress, self.storeSuburb, self.storeCity, self.storePostcode, self.storeLatitude, self.storeLongitude];
}

- (void)addCouponsObject:(CouponObject *)value
{
	if (self.coupons == nil)
	{
		[self setCoupons:[NSMutableSet setWithObjects:value, nil]];
	}
	else
	{
		[self.coupons addObject:value];
	}
}

- (void)removeCouponsObject:(CouponObject *)value
{
	[self.coupons removeObject:value];
}

@end
