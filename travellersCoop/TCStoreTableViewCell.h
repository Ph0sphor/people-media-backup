//
//  TCStoreTableViewCell.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 13/04/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCStoreTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *storeTitleLabel;
@property (nonatomic, strong) IBOutlet UILabel *storePhoneLabel;
@property (nonatomic, strong) IBOutlet UILabel *storeWebsiteLabel;
@property (nonatomic, strong) IBOutlet UILabel *storeAddressLabel;
@end
