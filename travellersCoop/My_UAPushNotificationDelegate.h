//
//  My_UAPushNotificationDelegate.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/11/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UAPush.h"

@protocol My_UAPushNotificationDelegate <UAPushNotificationDelegate>
- (void)receivedForegroundNotification:(NSDictionary *)notification
				fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

- (void)launchedFromNotification:(NSDictionary *)notification
		  fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;

- (void)receivedBackgroundNotification:(NSDictionary *)notification
				fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;
@end
