//
//  TCCouponContainerViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 27/11/13.
//  Copyright (c) 2013 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCouponDetailViewController.h"

@interface TCCouponContainerViewController : UIViewController
@property (nonatomic, strong) TCCouponDetailViewController *couponVC;
@property (nonatomic, strong) CouponObject *myCoupon;
@property (nonatomic, strong) UIColor *labelColor;
@end
