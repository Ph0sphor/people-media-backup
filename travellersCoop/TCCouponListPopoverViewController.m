//
//  TCCouponListPopoverViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 15/07/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCCouponListPopoverViewController.h"
#import "TCCouponListTableViewCell.h"

@interface TCCouponListPopoverViewController ()

@end

@implementation TCCouponListPopoverViewController

-(void)reloadData
{
	dispatch_async(dispatch_get_main_queue(), ^{
		[couponTableView reloadData];
	});
}

-(CGFloat)getNewTableHeight
{
	float cellHeight = 50.0;
	
	if (self.couponArray.count > 5)
	{
		return 5 * cellHeight;
	}
	else
	{
		return self.couponArray.count * cellHeight;
	}
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIImage *)maskColor:(UIColor *)color withMask:(UIImage *)maskImage
{
    if (color != nil)
	{
		const CGFloat *components = CGColorGetComponents([color CGColor]);
		CGFloat red = components[0];
		CGFloat green = components[1];
		CGFloat blue = components[2];
		CGFloat alpha = components[3];
		
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = CGBitmapContextCreate(NULL, maskImage.size.width, maskImage.size.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipLast);
		
		CGContextSetRGBFillColor(context, red, green, blue, alpha );
		
		CGContextAddRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		CGContextFillRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		
		CGImageRef solidImage = CGBitmapContextCreateImage(context);
		
		CGColorSpaceRelease(colorSpace);
		CGContextRelease(context);
		
		CGImageRef maskRef = maskImage.CGImage;
		
		CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
											CGImageGetHeight(maskRef),
											CGImageGetBitsPerComponent(maskRef),
											CGImageGetBitsPerPixel(maskRef),
											CGImageGetBytesPerRow(maskRef),
											CGImageGetDataProvider(maskRef), NULL, false);
		
		CGImageRef masked = CGImageCreateWithMask(solidImage, mask);
		CGImageRelease(solidImage);
		CGImageRelease(mask);
		
		UIImage *returnImage = [UIImage imageWithCGImage:masked];
		CGImageRelease(masked);
		return returnImage;
	}
    
    return nil;
}

#pragma mark - UITableView Data and Delegate Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.couponArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *CellIdentifier = @"couponListCellIdentifier";
	TCCouponListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	[cell.titleLabel setText:[(CouponObject *)[self.couponArray objectAtIndex:indexPath.row] couponTitle]];
	[cell.descriptionLabel setText:[(CouponObject *)[self.couponArray objectAtIndex:indexPath.row] couponDescription]];
	UIImage *image = [self maskColor:[(CouponObject *)[self.couponArray objectAtIndex:indexPath.row] categoryColor] withMask:[UIImage imageNamed:@"pinmask.png"]];
	[cell.thumbImage setImage:image];
	cell.backgroundColor = [UIColor clearColor];
	cell.contentView.backgroundColor = [UIColor clearColor];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	return cell;
	
	
	//static NSString *CellIdentifier = @"couponCellIdentifier";
	//TCCategoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	if ([self.delegate respondsToSelector:@selector(didSelectCoupon:)])
	{
		[self.delegate didSelectCoupon:[self.couponArray objectAtIndex:indexPath.row]];
	}
}


@end
