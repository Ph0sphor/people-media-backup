//
//  TCBranchFinderViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCBranchFinderViewController.h"
#import "AMIBranchObject.h"

@interface TCBranchFinderViewController ()

@end

@implementation TCBranchFinderViewController

-(IBAction)back:(id)sender
{
	[[self navigationController] popViewControllerAnimated:YES];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    // register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillBeShown:)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	
    [[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(keyboardWillBeHidden:)
												 name:UIKeyboardWillHideNotification
											   object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillShowNotification
												  object:nil];
	
    [[NSNotificationCenter defaultCenter] removeObserver:self
													name:UIKeyboardWillHideNotification
												  object:nil];
	
	[branchMapView removeAnnotations:branchMapView.annotations];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Branch Finder"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	
	for (AMIBranchObject *branchObject in self.branchArray)
	{
		[branchMapView addAnnotation:branchObject];
	}

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
	NSLog(@"TCBranchFinderViewController: dealloc");
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)performSearch
{
	NSString *trimmedSearchString = [branchSearchTextField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	NSMutableArray *resultsArray = [[NSMutableArray alloc] init];
	for (AMIBranchObject *branch in self.branchArray)
	{
		NSLog(@"branch: %@", [branch branchName]);
		if ([[branch branchName] rangeOfString:trimmedSearchString options:NSCaseInsensitiveSearch].location != NSNotFound)
		{
			[resultsArray addObject:branch];
		}
	}
	
	if (resultsArray.count > 0)
	{
		if (resultsArray.count == 1)
		{
			[self setCenterCoordinate:[(AMIBranchObject *)[resultsArray objectAtIndex:0] coordinate] zoomLevel:15 animated:YES];
		}
		else
		{
			[self zoomToFitTheseAnnotations:resultsArray];
		}
	}
	else if (userLocationFlag == YES)
	{
		[resultsArray addObjectsFromArray:self.branchArray];
		[resultsArray sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
			CLLocation *locA = [[CLLocation alloc] initWithLatitude:[(AMIBranchObject *)obj1 coordinate].latitude longitude:[(AMIBranchObject *)obj1 coordinate].longitude];
			CLLocation *locB = [[CLLocation alloc] initWithLatitude:[(AMIBranchObject *)obj2 coordinate].latitude longitude:[(AMIBranchObject *)obj2 coordinate].longitude];
			
			CLLocationDistance distanceA = [branchMapView.userLocation.location distanceFromLocation:locA];
			CLLocationDistance distanceB = [branchMapView.userLocation.location distanceFromLocation:locB];
			
			
			if (distanceA < distanceB)
			{
				return NSOrderedAscending;
			}
			else if (distanceA > distanceB)
			{
				return NSOrderedDescending;
			}
			else
			{
				return NSOrderedSame;
			}
			
		}];
		[self zoomToFitTheseAnnotations:[NSArray arrayWithObject:[resultsArray objectAtIndex:0]]];
		
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Results"
														message:@"Sorry, we could not find any branches matching your search text. Please try again, or delete your search text and press \"Search\" to manually search the map area for a branch."
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}
}

#pragma mark - UITextFieldDelegate Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	if (textField.text.length > 0)
	{
		[self performSearch];
	}
	return YES;
}

#pragma mark - Keyboard movement methods
- (void)keyboardWillBeShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
	UIViewAnimationCurve keyboardTransitionAnimationCurve;
	[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&keyboardTransitionAnimationCurve];
	NSNumber *kbAD = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];

		// bottom of text field is below the top of the keyboard.
	float difference = (self.view.frame.size.height - kbSize.height - branchSearchView.frame.size.height);
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:[kbAD floatValue]];
	[UIView setAnimationCurve:keyboardTransitionAnimationCurve];
	[branchSearchView setFrame:CGRectMake(0, difference, branchSearchView.frame.size.width, branchSearchView.frame.size.height)];
	[UIView commitAnimations];
	
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	NSDictionary* info = [aNotification userInfo];
	UIViewAnimationCurve keyboardTransitionAnimationCurve;
	[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&keyboardTransitionAnimationCurve];
	NSNumber *kbAD = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:[kbAD floatValue]];
	[UIView setAnimationCurve:keyboardTransitionAnimationCurve];
	[branchSearchView setFrame:CGRectMake(0, self.view.frame.size.height - branchSearchView.frame.size.height, branchSearchView.frame.size.width, branchSearchView.frame.size.height)];
	[UIView commitAnimations];
	
}

#pragma mark - MKMapView Methods

#define MERCATOR_RADIUS 85445659.44705395
#define MERCATOR_OFFSET 268435456
#define iphoneScaleFactorLatitude   9.0
#define iphoneScaleFactorLongitude  11.0
- (double)longitudeToPixelSpaceX:(double)longitude
{
    return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * M_PI / 180.0);
}

- (double)latitudeToPixelSpaceY:(double)latitude
{
    return round(MERCATOR_OFFSET - MERCATOR_RADIUS * logf((1 + sinf(latitude * M_PI / 180.0)) / (1 - sinf(latitude * M_PI / 180.0))) / 2.0);
}

- (double)pixelSpaceXToLongitude:(double)pixelX
{
    return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / M_PI;
}

- (double)pixelSpaceYToLatitude:(double)pixelY
{
    return (M_PI / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / M_PI;
}

- (MKCoordinateSpan)coordinateSpanWithMapView:(MKMapView *)amapView
                             centerCoordinate:(CLLocationCoordinate2D)centerCoordinate
                                 andZoomLevel:(NSUInteger)zoomLevel
{
    // convert center coordiate to pixel space
    double centerPixelX = [self longitudeToPixelSpaceX:centerCoordinate.longitude];
    double centerPixelY = [self latitudeToPixelSpaceY:centerCoordinate.latitude];
    
    // determine the scale value from the zoom level
    NSInteger zoomExponent = 20 - zoomLevel;
    double zoomScale = pow(2, zoomExponent);
    
    // scale the map’s size in pixel space
    CGSize mapSizeInPixels = amapView.bounds.size;
    double scaledMapWidth = mapSizeInPixels.width * zoomScale;
    double scaledMapHeight = mapSizeInPixels.height * zoomScale;
    
    // figure out the position of the top-left pixel
    double topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
    double topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
    
    // find delta between left and right longitudes
    CLLocationDegrees minLng = [self pixelSpaceXToLongitude:topLeftPixelX];
    CLLocationDegrees maxLng = [self pixelSpaceXToLongitude:topLeftPixelX + scaledMapWidth];
    CLLocationDegrees longitudeDelta = maxLng - minLng;
    
    // find delta between top and bottom latitudes
    CLLocationDegrees minLat = [self pixelSpaceYToLatitude:topLeftPixelY];
    CLLocationDegrees maxLat = [self pixelSpaceYToLatitude:topLeftPixelY + scaledMapHeight];
    CLLocationDegrees latitudeDelta = -1 * (maxLat - minLat);
    
    // create and return the lat/lng span
    MKCoordinateSpan span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta);
    return span;
}

- (void)setCenterCoordinate:(CLLocationCoordinate2D)centerCoordinate
                  zoomLevel:(NSUInteger)zoomLevel
                   animated:(BOOL)animated
{
    
    // clamp large numbers to 28
    zoomLevel = MIN(zoomLevel, 28);
    
    // use the zoom level to compute the region
    MKCoordinateSpan span = [self coordinateSpanWithMapView:branchMapView centerCoordinate:centerCoordinate andZoomLevel:zoomLevel];
    MKCoordinateRegion region = MKCoordinateRegionMake(centerCoordinate, span);
    
    // set the region like normal
    [branchMapView setRegion:region animated:animated];
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	if (userLocationFlag == NO)
	{
		[self setCenterCoordinate:userLocation.coordinate zoomLevel:15 animated:YES];
	}
	
	userLocationFlag = YES;
}

-(void)zoomToFitTheseAnnotations:(NSArray *)annotations
{
    if([annotations count] == 0)
		return;
	
    CLLocationCoordinate2D topLeftCoord;
    topLeftCoord.latitude = -90;
    topLeftCoord.longitude = 180;
	
    CLLocationCoordinate2D bottomRightCoord;
    bottomRightCoord.latitude = 90;
    bottomRightCoord.longitude = -180;
	
	
	
    for(AMIBranchObject* annotation in annotations)
    {
		topLeftCoord.longitude = fmin(topLeftCoord.longitude, annotation.coordinate.longitude);
		topLeftCoord.latitude = fmax(topLeftCoord.latitude, annotation.coordinate.latitude);
        
		bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, annotation.coordinate.longitude);
		bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, annotation.coordinate.latitude);
    }
	
	if (userLocationFlag == YES) {
		topLeftCoord.longitude = fmin(topLeftCoord.longitude, branchMapView.userLocation.location.coordinate.longitude);
		topLeftCoord.latitude = fmax(topLeftCoord.latitude, branchMapView.userLocation.location.coordinate.latitude);
		
		bottomRightCoord.longitude = fmax(bottomRightCoord.longitude, branchMapView.userLocation.location.coordinate.longitude);
		bottomRightCoord.latitude = fmin(bottomRightCoord.latitude, branchMapView.userLocation.location.coordinate.latitude);
	}
    
	
    MKCoordinateRegion region;
    region.center.latitude = topLeftCoord.latitude - (topLeftCoord.latitude - bottomRightCoord.latitude) * 0.5;
	if (region.center.latitude < -90)
	{
		region.center.latitude = -90;
	}
	if (region.center.latitude > 90)
	{
		region.center.latitude = 90;
	}
    region.center.longitude = topLeftCoord.longitude + (bottomRightCoord.longitude - topLeftCoord.longitude) * 0.5;
	
	if (region.center.longitude < -180)
	{
		region.center.longitude = -180;
	}
	if (region.center.longitude > 180)
	{
		region.center.longitude = 180;
	}
	
    region.span.latitudeDelta = fabs((topLeftCoord.latitude - bottomRightCoord.latitude) * 1.5); // Add a little extra space on the sides
    region.span.longitudeDelta = fabs((bottomRightCoord.longitude - topLeftCoord.longitude) * 1.5); // Add a little extra space on the sides
	
	
    region = [branchMapView regionThatFits:region];
    [branchMapView setRegion:region animated:YES];
}


- (MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation
{
    
    // if it's the user location, just return nil.
    if ([annotation isKindOfClass:[MKUserLocation class]])
    {
        return nil;
    }
	else
    {
        MKAnnotationView *annotationView = nil;
        if ([annotation isKindOfClass:[AMIBranchObject class]])
        {
			static NSString *ImageIdentifier = @"ImageIdentifier";
			MKAnnotationView *storeView = [mV dequeueReusableAnnotationViewWithIdentifier:ImageIdentifier];
			if (storeView == nil)
			{
				storeView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:ImageIdentifier];
			}
			storeView.enabled = YES;
			storeView.canShowCallout = YES;
			[storeView setImage:[UIImage imageNamed:@"store.png"]];
			annotationView = storeView;
		}
		return annotationView;
	}
}

@end
