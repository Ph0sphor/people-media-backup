//
//  CategoryObject.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 31/07/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "CategoryObject.h"

@implementation CategoryObject

-(id)init
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

-(NSString *)getCategoryTitle
{
	return self.categoryName;
}

-(NSString *)description
{
	return [NSString stringWithFormat:@"id: %d, name: %@, parentID: %d, vendorID: %d", self.categoryID, self.categoryName, self.parentID, self.vendorID];
}

@end
