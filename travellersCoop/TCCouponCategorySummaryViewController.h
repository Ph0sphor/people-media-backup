//
//  TCCouponCategorySummaryViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 14/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TCCouponDetailViewController.h"
#import "CouponObject.h"

@interface TCCouponCategorySummaryViewController : UIViewController <UIGestureRecognizerDelegate>
{
	IBOutlet UIView *couponView;
	IBOutlet UILabel *messageLabel;
	IBOutlet UIButton *leftButton;
	IBOutlet UIButton *rightButton;
	__strong NSArray *couponArray;
	int currentPage;
}
-(IBAction)back:(id)sender;
-(IBAction)lastCoupon:(id)sender;
-(IBAction)nextCoupon:(id)sender;
-(void)setCouponArray:(NSArray *)coupons;
@property (nonatomic, strong) TCCouponDetailViewController *currentDetailVC;
@property (nonatomic, strong) UISwipeGestureRecognizer *leftSwipeRecognizer;
@property (nonatomic, strong) UISwipeGestureRecognizer *rightSwipeRecognizer;
@end
