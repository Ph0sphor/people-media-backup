//
//  TCLoginNoAnimationSegue.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 27/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCLoginNoAnimationSegue : UIStoryboardSegue

@end
