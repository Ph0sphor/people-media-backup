//
//  TCOfferTableViewCell.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/07/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponObject.h"

@interface TCOfferTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *bgImage;
@property (nonatomic, strong) CouponObject *myCoupon;
@end
