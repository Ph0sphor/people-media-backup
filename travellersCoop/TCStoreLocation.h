//
//  TCStoreLocation.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 21/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class CouponObject;

@interface TCStoreLocation : NSObject <MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSString *storeName;
@property (nonatomic, strong) NSString *storeStreet;
@property (nonatomic, strong) NSString *storeSuburb;
@property (nonatomic, strong) NSString *storeRegion;
@property (nonatomic, strong) NSArray *coupons;

-(NSString *)title;
-(NSString *)subtitle;
-(id)initWithCoordinate:(CLLocationCoordinate2D) c;
-(CLLocationCoordinate2D)getCoordinate;
@end

