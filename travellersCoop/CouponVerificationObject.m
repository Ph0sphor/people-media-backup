//
//  CouponVerificationObject.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 31/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "CouponVerificationObject.h"


@implementation CouponVerificationObject

-(id)init
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

-(NSString *)description
{
    return [NSString stringWithFormat:@"couponID: %@, storeID: %@, valueUsed: %@, timestamp: %@", self.couponID, self.storeID, self.valueUsed, self.timestamp];
}

-(void)encodeWithCoder:(NSCoder*)encoder {
    [encoder encodeObject:self.couponID forKey:@"couponID"];
    [encoder encodeObject:self.timestamp forKey:@"timestamp"];
    [encoder encodeObject:self.valueUsed forKey:@"valueUsed"];
    [encoder encodeObject:self.storeCode forKey:@"storeCode"];
    [encoder encodeObject:self.storeID forKey:@"storeID"];
	[encoder encodeObject:[NSNumber numberWithBool:self.isSingleUse] forKey:@"isSingleUse"];
}

-(id)initWithCoder:(NSCoder*)decoder {
	self = [super init];
	if (self) {
		self.couponID = [decoder decodeObjectForKey:@"couponID"];
		self.timestamp = [decoder decodeObjectForKey:@"timestamp"];
		self.valueUsed = [decoder decodeObjectForKey:@"valueUsed"];
		self.storeCode = [decoder decodeObjectForKey:@"storeCode"];
		self.storeID = [decoder decodeObjectForKey:@"storeID"];
		self.isSingleUse = [[decoder decodeObjectForKey:@"isSingleUse"] boolValue];
	}
	return self;
}

@end
