//
//  TCAppDelegate.h
//  travellersCoop
//
//  Created by Scott Judson on 7/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "SDWebImage/SDWebImage/SDWebImageDownloader.h"
#import "UAirship.h"
#import "UAConfig.h"
#import "UAPush.h"
#import "UALocationService.h"
#import "My_UAPushNotificationDelegate.h"



#define IS_OFFLINE_MODE 0
#define kAppOpenedEvent @"App Opened"
#define kAppOpenedEventDescription @"Customer opened application."
#define kOfferViewedEvent @"Offer - %@"
#define kOfferViewedEventDescription @"Customer viewed an offer."
#define kGeoPushTriggeredEvent @"Geo Push Triggered"
#define kGeoPushTriggeredEventDescription @"Customer received geo location based push notification."

@class TCMainMenuViewController;
@class CategoryObject;
@class CouponObject;
@class StoreObject;
@class RegionObject;
@class CouponVerificationObject;

@interface TCAppDelegate : UIResponder <UIApplicationDelegate, CLLocationManagerDelegate,  UALocationServiceDelegate, UAPushNotificationDelegate>
{
    int totalImages;
    int imageDownloads;
    int totalDownloads;
    int currentDownloadCount;
	BOOL firstDataLaunch;
	BOOL gotToken;
	
	
	CLLocation *currentLocation;
}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) UINavigationController *navController;
@property (nonatomic, assign) BOOL isRetinaDisplay;
@property (nonatomic, assign) BOOL isSendingCoupons;
@property (nonatomic, strong) NSMutableArray *downloadingArray;
@property (nonatomic, strong) NSMutableArray *downloaderArray;
@property(nonatomic, strong) id<GAITracker> tracker;
@property (nonatomic, strong) CLLocationManager *locationManager;
-(NSString *)getVendorID;
- (NSString *)applicationDocumentsDirectory;
-(CLLocation *)getCurrentLocation;
-(void)getCategories:(void (^)(NSArray *))completion;
-(void)getStores:(void (^)(NSArray *))completion;
-(void)getCoupons:(void (^)(NSArray *))completion;
-(void)getCouponsForCategory:(int)categoryID withCompletion:(void (^)(NSArray *))completion;
-(void)sendAnyVerifiedCoupons;
-(void)loadBranchDataFromCSVwithCompletion:(void (^)(NSArray *))completion;
-(void)validateKPIDwithCompletionBlock:(void (^)(BOOL))completion;
-(void)loginWithLastName:(NSString *)lastName withRegistration:(NSString *)registration withCompletionBlock:(void (^)(BOOL))completion;
-(void)loginWithPolicyNumber:(NSString *)policyNumber withCompletionBlock:(void (^)(BOOL))completion;
-(void)loginThreeWithLastName:(NSString *)lastName withRegistration:(NSString *)registration withCompletionBlock:(void (^)(BOOL))completion;
-(void)loginWithLastName:(NSString *)lastName withRegistration:(NSString *)registration asynchronous:(BOOL)asynchronous withCompletionBlock:(void (^)(BOOL))completion;
-(void)sendUbiquityEvent:(NSString *)eventType withDescription:(NSString *)description;
@end
