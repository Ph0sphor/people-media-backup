//
//  TCCategoryTableViewCell.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 24/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryObject.h"

@interface TCCategoryTableViewCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *bgImage;
@property (nonatomic, strong) CategoryObject *myCategory;
@end
