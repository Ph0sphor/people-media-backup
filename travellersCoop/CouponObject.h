//
//  CouponObject.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 18/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "StoreObject.h"

@interface CouponObject : NSObject

@property (nonatomic, strong) NSString * categoryID;
@property (nonatomic, strong) UIColor *categoryColor;
@property (nonatomic, strong) NSString * couponExtraInfo;
@property (nonatomic, assign) int couponID;
@property (nonatomic, strong) NSNumber * loyaltyThreshold;
@property (nonatomic, strong) NSString * couponParentID;
@property (nonatomic, strong) NSString * couponDescription;
@property (nonatomic, strong) NSString * couponParentType;
@property (nonatomic, strong) NSNumber * couponIsRedeemable;
@property (nonatomic, strong) NSNumber * couponIsRepeat;
@property (nonatomic, strong) NSDate * couponRepeatStartDate;
@property (nonatomic, strong) NSDate * couponRepeatEndDate;
@property (nonatomic, strong) NSString * couponTermsConditions;
@property (nonatomic, strong) NSString * couponTitle;
@property (nonatomic, strong) NSString * couponTypeID;
@property (nonatomic, strong) NSString * couponTypeDescription;
@property (nonatomic, strong) NSString * couponValue;
@property (nonatomic, strong) NSString * couponImageURL;
@property (nonatomic, strong) NSString * couponImageURLRetina;
@property (nonatomic, strong) NSString * loyaltyRewardCouponID;
@property (nonatomic, strong) NSArray *storeArray;

@end
