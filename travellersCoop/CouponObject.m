//
//  CouponObject.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 18/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "CouponObject.h"

@implementation CouponObject

-(id)init
{
	self = [super init];
	if (self) {
		
	}
	return self;
}

-(NSString *)description
{
	return [NSString stringWithFormat:@"COUPON: id: %d,  %@", self.couponID, self.couponTitle];
}

@end
