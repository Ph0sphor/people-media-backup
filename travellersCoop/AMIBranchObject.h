//
//  AMIBranchObject.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 22/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface AMIBranchObject : NSObject <MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, strong) NSString *branchName;
@property (nonatomic, strong) NSString *branchStreet;
@property (nonatomic, strong) NSString *branchSuburb;
@property (nonatomic, strong) NSString *branchRegion;
@property (nonatomic, strong) NSString *branchPostcode;
@property (nonatomic, strong) NSString *branchPhone;
@property (nonatomic, strong) NSString *branchEmail;

-(NSString *)title;
-(NSString *)subtitle;
-(id)initWithCoordinate:(CLLocationCoordinate2D) c;
-(CLLocationCoordinate2D)getCoordinate;

@end
