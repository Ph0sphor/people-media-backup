//
//  AMIBranchObject.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 22/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "AMIBranchObject.h"

@implementation AMIBranchObject
@synthesize coordinate, branchName, branchStreet, branchSuburb, branchRegion, branchPostcode, branchEmail, branchPhone;

-(NSString *)description
{
	return [NSString stringWithFormat:@"%@: %f, %f", self.branchName, self.coordinate.latitude, self.coordinate.longitude];
}

- (NSString *)subtitle
{
	if ([self.branchSuburb isEqualToString:@""])
	{
		return [NSString stringWithFormat:@"%@, %@ %@", branchStreet, branchRegion, branchPostcode];
	}
	else
	{
		return [NSString stringWithFormat:@"%@, %@, %@ %@", branchStreet, branchSuburb, branchRegion, branchPostcode];
	}
}

- (NSString *)title
{
    return self.branchName;
}

-(CLLocationCoordinate2D)getCoordinate{
    return self.coordinate;
}

-(id)initWithCoordinate:(CLLocationCoordinate2D)c
{
	
    self = [super init];
    if (self)
    {
        self.coordinate=c;
    }
	return self;
}

-(void)dealloc
{
    self.branchName = nil;
    self.branchStreet = nil;
    self.branchSuburb = nil;
    self.branchRegion = nil;
    self.branchPostcode = nil;
	self.branchEmail = nil;
	self.branchPhone = nil;
}
@end
