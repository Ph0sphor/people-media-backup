//
//  TCCouponDetailViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 16/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "SDWebImage/SDWebImage/UIImageView+WebCache.h"

@class CouponObject;

@interface TCCouponDetailViewController : UIViewController <MFMailComposeViewControllerDelegate, UIScrollViewDelegate, UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>
{
    IBOutlet UIScrollView *detailScrollView;
    IBOutlet UIImageView *detailImageView;
	IBOutlet UIImageView *detailTapImageView;
    IBOutlet UIView *termsAndConditionsView;
    
    IBOutlet UIView *storeInfoView;
	IBOutlet UIButton *callButton;
	IBOutlet UILabel *phoneNumberLabel;
	IBOutlet UIButton *mapButton;
	IBOutlet UILabel *addressLabel;
	IBOutlet UIButton *websiteButton;
	IBOutlet UILabel *urlLabel;
	
	
	IBOutlet UITableView *storeTableView;
    int storeIndex;
    
    
    IBOutlet UILabel *termsConditionsLabel;
    IBOutlet UILabel *descriptionLabel;
    
}
@property (nonatomic, strong) IBOutlet UIView *contentView;
@property (nonatomic, strong) CouponObject *myCoupon;
-(IBAction)help:(id)sender;
-(BOOL)back;
-(IBAction)callCompany:(id)sender;
-(IBAction)showCompanyOnMap:(id)sender;
-(IBAction)viewCompanyWebsite:(id)sender;
-(IBAction)showTermsAndConditions:(id)sender;
@end
