//
//  TCCouponCategorySummaryViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 14/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCCouponCategorySummaryViewController.h"
#import "TCAppDelegate.h"
#import "CouponVerificationObject.h"
@interface TCCouponCategorySummaryViewController ()

@end

@implementation TCCouponCategorySummaryViewController

-(IBAction)back:(id)sender
{
	if (self.currentDetailVC != nil)
	{
		if ([self.currentDetailVC back] == YES)
		{
			[[self navigationController] popViewControllerAnimated:YES];
		}
	}
	else
	{
		[[self navigationController] popViewControllerAnimated:YES];
	}
}

-(IBAction)lastCoupon:(id)sender
{
	[self swipeRight:nil];
}

-(IBAction)nextCoupon:(id)sender
{
	[self swipeLeft:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	self.leftSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeLeft:)];
	self.leftSwipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
	self.leftSwipeRecognizer.delegate = self;
	[couponView addGestureRecognizer:self.leftSwipeRecognizer];
	
	self.rightSwipeRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeRight:)];
	self.rightSwipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
	self.rightSwipeRecognizer.delegate = self;
	[couponView addGestureRecognizer:self.rightSwipeRecognizer];
	currentPage = 0;
	if (couponArray.count > 0)
	{
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
		TCCouponDetailViewController *couponVC = [storyboard instantiateViewControllerWithIdentifier:@"CouponDetailViewControllerIdentifier"];
		self.currentDetailVC = couponVC;
		[self.currentDetailVC setMyCoupon:[couponArray objectAtIndex:0]];
		[self.currentDetailVC.view setFrame:CGRectMake(0, 0, couponView.frame.size.width, couponView.frame.size.height)];
		[self.currentDetailVC beginAppearanceTransition:YES animated:YES];
		[couponView addSubview:self.currentDetailVC.view];
		
		if (couponArray.count > 1)
		{
			[leftButton setHidden:NO];
			[rightButton setHidden:NO];
		}
		else
		{
			[leftButton setHidden:YES];
			[rightButton setHidden:YES];
		}
	}
	else
	{
		[messageLabel setText:@"loading..."];
		[leftButton setHidden:YES];
		[rightButton setHidden:YES];
	}
}

-(void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didVerifySingleUseCoupon:) name:kDidVerifySingleUseCouponNotification object:nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)didVerifySingleUseCoupon:(NSNotification *)notification
{
	NSDictionary *userInfo = notification.userInfo;
	int cateogry = [self.currentDetailVC.myCoupon.categoryID intValue];
	CouponVerificationObject *couponVO = [userInfo objectForKey:@"couponVerificationObject"];
	if ([couponVO.categoryID intValue] == cateogry)
	{
		[self.currentDetailVC.view removeFromSuperview];
		self.currentDetailVC = nil;
		TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
		[appDelegate getCouponsForCategory:cateogry withCompletion:^(NSArray *newCouponArray) {
			[self setCouponArray:newCouponArray];
		}];

	}
}

-(void)setCouponArray:(NSArray *)coupons
{
	if (couponArray != nil && self.currentDetailVC != nil)
	{
		[self.currentDetailVC.view removeFromSuperview];
		self.currentDetailVC = nil;
	}
	
	NSMutableArray *couponArrayOne = [[NSMutableArray alloc] init];
	for (CouponObject *coupon in coupons)
	{
		if (couponArrayOne.count == 0)
		{
			[couponArrayOne addObject:coupon];
		}
		else
		{
			for (CouponObject *couponObj in coupons)
			{
				if (![couponArrayOne containsObject:couponObj] && [(CouponObject *)[couponArrayOne lastObject] couponID] != [couponObj couponID])
				{
					[couponArrayOne addObject:couponObj];
					break;
				}
			}
		}
	}
	
	if (couponArrayOne.count == 1)
	{
		couponArray = coupons;
	}
	else
	{
		couponArray = couponArrayOne;
	}
	
	
	if (couponArray.count > 0)
	{
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
		TCCouponDetailViewController *couponVC = [storyboard instantiateViewControllerWithIdentifier:@"CouponDetailViewControllerIdentifier"];
		self.currentDetailVC = couponVC;
		[self.currentDetailVC setMyCoupon:[couponArray objectAtIndex:0]];
		
		[self.currentDetailVC.view setFrame:CGRectMake(0, 0, couponView.frame.size.width, couponView.frame.size.height)];
		[self.currentDetailVC beginAppearanceTransition:YES animated:YES];
		[couponView addSubview:self.currentDetailVC.view];
		if (couponArray.count > 1)
		{
			[leftButton setHidden:NO];
			[rightButton setHidden:NO];
		}
		else
		{
			[leftButton setHidden:YES];
			[rightButton setHidden:YES];
		}
		
	}
	else
	{
		[leftButton setHidden:YES];
		[rightButton setHidden:YES];
		[messageLabel setText:@"Sorry, that offer is not available at the moment. Please check back soon."];
	}
}
	
-(void)swipeLeft:(UISwipeGestureRecognizer *)sender
{
	// show next
	NSLog(@"swipeLeft");
	if ((currentPage + 1 < couponArray.count) || (couponArray.count > 1 && currentPage + 1 == couponArray.count))
	{
		CouponObject *nextCoupon = nil;
		if (currentPage + 1 == couponArray.count)
		{
			nextCoupon = [couponArray objectAtIndex:0];
		}
		else
		{
			nextCoupon = [couponArray objectAtIndex:currentPage + 1];
		}
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
		TCCouponDetailViewController *nextVC = [storyboard instantiateViewControllerWithIdentifier:@"CouponDetailViewControllerIdentifier"];
		[nextVC setMyCoupon:nextCoupon];
		
		[nextVC.view setFrame:CGRectMake(couponView.frame.size.width, 0, couponView.frame.size.width, couponView.frame.size.height)];
		[nextVC beginAppearanceTransition:YES animated:YES];
		[couponView addSubview:nextVC.view];
		
		[UIView animateWithDuration:0.3
						 animations:^{
							 [self.currentDetailVC.view setFrame:CGRectMake(self.currentDetailVC.view.frame.origin.x - self.currentDetailVC.view.frame.size.width, self.currentDetailVC.view.frame.origin.y, self.currentDetailVC.view.frame.size.width, self.currentDetailVC.view.frame.size.height)];
							 [nextVC.view setFrame:CGRectMake(0, 0, nextVC.view.frame.size.width, nextVC.view.frame.size.height)];
						 }
						 completion:^(BOOL finished) {
							 [self.currentDetailVC.view removeFromSuperview];
							 self.currentDetailVC = nextVC;
							 if (currentPage + 1 == couponArray.count)
							 {
								 currentPage = 0;
							 }
							 else
							 {
								 currentPage++;
							 }
							 
						 }];
		
		
	}
}

-(void)swipeRight:(UISwipeGestureRecognizer *)sender
{
	// show previous
	NSLog(@"swipeRight");
	if ((currentPage - 1 >= 0) || (currentPage == 0 && couponArray.count > 1))
	{
		CouponObject *prevCoupon = nil;
		if (currentPage == 0)
		{
			prevCoupon = [couponArray lastObject];
		}
		else
		{
			prevCoupon = [couponArray objectAtIndex:currentPage - 1];
		}
		
		UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
		TCCouponDetailViewController *previousVC = [storyboard instantiateViewControllerWithIdentifier:@"CouponDetailViewControllerIdentifier"];
		[previousVC setMyCoupon:prevCoupon];
		[previousVC.view setFrame:CGRectMake(0 - couponView.frame.size.width, 0, couponView.frame.size.width, couponView.frame.size.height)];
		[previousVC beginAppearanceTransition:YES animated:YES];
		[couponView addSubview:previousVC.view];
		
		[UIView animateWithDuration:0.3
						 animations:^{
							 [self.currentDetailVC.view setFrame:CGRectMake(self.currentDetailVC.view.frame.origin.x + self.currentDetailVC.view.frame.size.width, self.currentDetailVC.view.frame.origin.y, self.currentDetailVC.view.frame.size.width, self.currentDetailVC.view.frame.size.height)];
							 [previousVC.view setFrame:CGRectMake(0, 0, previousVC.view.frame.size.width, previousVC.view.frame.size.height)];
						 }
						 completion:^(BOOL finished) {
							 [self.currentDetailVC.view removeFromSuperview];
							 self.currentDetailVC = previousVC;
							 if (currentPage == 0)
							 {
								 currentPage = couponArray.count - 1.0;
							 }
							 else
							 {
								 currentPage--;
							 }
							 
						 }];
	}
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
