//
//  SliderCategoryButtonView.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SliderCategoryButtonViewDelegate;

@interface SliderCategoryButtonView : UIView

@property (nonatomic, assign) id<SliderCategoryButtonViewDelegate> delegate;
@property (nonatomic, strong) UIColor *categoryColor;
@property (nonatomic, strong) IBOutlet UILabel *buttonTitleLabel;
@property (nonatomic, strong) IBOutlet UIImageView *buttonImageView;
@property (nonatomic, assign) int categoryID;

-(IBAction)toggleSelected:(id)sender;

@end

@protocol SliderCategoryButtonViewDelegate <NSObject>
-(void)setCategoryWithID:(int)categoryID selected:(BOOL)selected;
@end