//
//  TCSavedView.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 23/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "TCSavedView.h"

@implementation TCSavedView
@synthesize imageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

-(void)setupImage
{
    CGColorRef color = [self.imageView.backgroundColor CGColor];
    self.imageView.backgroundColor = [UIColor clearColor];
    
    
    const CGFloat *components = CGColorGetComponents(color);
    CGFloat red = components[0];
    CGFloat green = components[1];
    CGFloat blue = components[2];
    CGFloat alpha = components[3];

    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, 150, 150, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipLast);
    
    CGContextSetRGBFillColor(context, red, green, blue, alpha );
    
    CGContextAddRect(context, CGRectMake(0, 0, 150, 150));
    CGContextFillRect(context, CGRectMake(0, 0, 150, 150));
    
    CGImageRef solidImage = CGBitmapContextCreateImage(context);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    
    [self.imageView setImage:[self maskImage:[UIImage imageWithCGImage:solidImage] withMask:[UIImage imageNamed:@"tickmask.png"]]];
    CGImageRelease(solidImage);
    //NSLog(@"image: %@", self.imageView.image);
}

-(UIImage *)maskImage:(UIImage *)theImage withMask:(UIImage *)maskImage
{
    
    CGImageRef maskRef = maskImage.CGImage; 
    
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
	CGImageRef masked = CGImageCreateWithMask([theImage CGImage], mask);
    CGImageRelease(mask);
    
    UIImage *returnImage = [UIImage imageWithCGImage:masked];
    CGImageRelease(masked);
	return returnImage;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
/*- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    
}*/


-(void)dealloc
{
    self.imageView = nil;
}

@end
