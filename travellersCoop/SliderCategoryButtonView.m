//
//  SliderCategoryButtonView.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 17/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import "SliderCategoryButtonView.h"

@implementation SliderCategoryButtonView
@synthesize delegate, buttonTitleLabel, buttonImageView, categoryID;

-(void)dealloc
{
    self.delegate = nil;
    self.buttonImageView = nil;
    self.buttonTitleLabel = nil;
}

-(UIImage *)maskColor:(UIColor *)color withMask:(UIImage *)maskImage
{
    if (color != nil)
	{
		const CGFloat *components = CGColorGetComponents([color CGColor]);
		CGFloat red = components[0];
		CGFloat green = components[1];
		CGFloat blue = components[2];
		CGFloat alpha = components[3];
		
		CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
		CGContextRef context = CGBitmapContextCreate(NULL, maskImage.size.width, maskImage.size.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaNoneSkipLast);
		
		CGContextSetRGBFillColor(context, red, green, blue, alpha );
		
		CGContextAddRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		CGContextFillRect(context, CGRectMake(0, 0, maskImage.size.width, maskImage.size.height));
		
		CGImageRef solidImage = CGBitmapContextCreateImage(context);
		
		CGColorSpaceRelease(colorSpace);
		CGContextRelease(context);
		
		CGImageRef maskRef = maskImage.CGImage;
		
		CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
											CGImageGetHeight(maskRef),
											CGImageGetBitsPerComponent(maskRef),
											CGImageGetBitsPerPixel(maskRef),
											CGImageGetBytesPerRow(maskRef),
											CGImageGetDataProvider(maskRef), NULL, false);
		
		CGImageRef masked = CGImageCreateWithMask(solidImage, mask);
		CGImageRelease(solidImage);
		CGImageRelease(mask);
		
		UIImage *returnImage = [UIImage imageWithCGImage:masked];
		CGImageRelease(masked);
		return returnImage;
	}
    
    return nil;
}

-(IBAction)toggleSelected:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(setCategoryWithID:selected:)])
    {
        if (self.buttonImageView.image == nil)
        {
			UIImage *image = [self maskColor:self.categoryColor withMask:[UIImage imageNamed:@"colour-icon-tick-box-mask-2.png"]];
            [self.buttonImageView setImage:image];
			self.buttonImageView.backgroundColor = [UIColor whiteColor];
            [self.delegate setCategoryWithID:self.categoryID selected:YES];
        }
        else
        {
            [self.buttonImageView setImage:nil];
			[self.buttonImageView setBackgroundColor:self.categoryColor];
            [self.delegate setCategoryWithID:self.categoryID selected:NO];
        }
    }
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



@end
