//
//  TCLoginViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCLoginViewController.h"
#import "ASIFormDataRequest.h"

@interface TCLoginViewController ()

@end

@implementation TCLoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

static NSString *cutoffurlString = @"http://my.koup.in/ios/?object=vendors&action=get_status&vendor_id=%@";
-(void)checkForCutoffWithCompletionBlock:(void (^)(BOOL))completion
{
	NSLog(@"checkForCutoffWithCompletionBlock:");
	TCAppDelegate *delegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
	ASIFormDataRequest *cutoffRequest = [[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:cutoffurlString, [delegate getVendorID]]]];
	
	__weak ASIFormDataRequest *cutoffRequest_b = cutoffRequest;
	[cutoffRequest setFailedBlock:^{
		NSLog(@"cutoffRequestFailed: %@", [cutoffRequest_b responseString]);
		completion(NO);
	}];
	[cutoffRequest setCompletionBlock:^{
		NSLog(@"cutoffRequestCompleted: %@", [cutoffRequest_b responseString]);
		NSError* error;
		NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:[cutoffRequest_b responseData] options:kNilOptions error:&error];
		if (error != nil)
		{
			// ERROR
			//NSLog(@"coupon request error: %@", [error localizedDescription]);
			completion(NO);
		}
		else
		{
			if ([[dataDict objectForKey:@"is_active"] isEqualToString:@"yes"])
			{
				completion(NO);
			}
			else
			{
				if (![[dataDict objectForKey:@"expiry_message"] isEqual:[NSNull null]])
				{
					[changeoverMessageLabel setText:[dataDict objectForKey:@"expiry_message"]];
				}
				else
				{
					[changeoverMessageLabel setText:@""];
				}
				
				[changeoverMessageLabel sizeToFit];
				
				if (![[dataDict objectForKey:@"expiry_url"] isEqual:[NSNull null]])
				{
					changeoverURL = [NSURL URLWithString:[dataDict objectForKey:@"expiry_url"]];
				}
				completion(YES);
			}
		}
	}];
	[cutoffRequest startSynchronous];
}

-(IBAction)launchURL:(id)sender
{
	if (changeoverURL != nil)
	{
		[[UIApplication sharedApplication] openURL:changeoverURL];
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	[backButton setHidden:YES];
	currentlyShowingView = welcomeView;
	
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Login"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    // register for keyboard notifications
	
	if (didCheckChangeover != YES)
	{
		didCheckChangeover = YES;
		[self checkForCutoffWithCompletionBlock:^(BOOL cutoff)
		 {
			 if (cutoff == NO)
			 {
				 [changeoverView setHidden:YES];
				 if ([[NSUserDefaults standardUserDefaults] objectForKey:@"kpid"] != nil)
				 {
					 TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
					 [appDelegate validateKPIDwithCompletionBlock:^(BOOL sucess) {
						 if (sucess == YES)
						 {
							 [self performSegueWithIdentifier:@"loginNoAnimationSegue" sender:self];
						 }
						 else
						 {
							 // show new can't login screen....
							 [welcomeView setHidden:NO];
							 [policyView setHidden:YES];
							 [policyNotFoundView setHidden:YES];
							 [policyGoButton setHidden:YES];
							 [backButton setHidden:NO];
							 currentlyShowingView = welcomeView;
						 }
					 }];
				 }
				 else
				 {
					 [[NSNotificationCenter defaultCenter] addObserver:self
															  selector:@selector(keyboardWillBeShown:)
																  name:UIKeyboardWillShowNotification
																object:nil];
					 
					 [[NSNotificationCenter defaultCenter] addObserver:self
															  selector:@selector(keyboardWillBeHidden:)
																  name:UIKeyboardWillHideNotification
																object:nil];
				 }
			 }
			 else
			 {
				 [changeoverView setHidden:NO];
			 }
		 }];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
	didCheckChangeover = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showPolicyNotFound
{
	if (policyView.isHidden == YES)
	{
		if (activeTextField != nil)
		{
			[activeTextField resignFirstResponder];
		}
		[policyView setAlpha:0.0];
		[policyView setHidden:NO];
		[policyNotFoundView setHidden:NO];
		[policyGoButton setHidden:YES];
		[UIView animateWithDuration:0.3
							  delay:0
							options:UIViewAnimationOptionCurveEaseIn
						 animations:^{
							 [currentlyShowingView setAlpha:0.0];
						 }
						 completion:^(BOOL finished) {
							 [UIView animateWithDuration:0.3
												   delay:0
												 options:UIViewAnimationOptionCurveEaseOut
											  animations:^{
												  [policyView setAlpha:1.0];
											  }
											  completion:^(BOOL finished) {
												  [currentlyShowingView setHidden:YES];
												  [currentlyShowingView setAlpha:1.0];
												  [backButton setHidden:NO];
											  }];
						 }];
	}
	else
	{
		if (activeTextField != nil)
		{
			[activeTextField resignFirstResponder];
		}
		[policyNotFoundView setAlpha:0.0];
		[policyNotFoundView setHidden:NO];
		[policyGoButton setHidden:YES];
		[UIView animateWithDuration:0.3
							  delay:0
							options:UIViewAnimationOptionCurveEaseIn
						 animations:^{
							 [policyNotFoundView setAlpha:1.0];
						 }
						 completion:^(BOOL finished) {
							 id tracker = [[GAI sharedInstance] defaultTracker];
							 [tracker set:kGAIScreenName value:@"Policy Number Not Found"];
							 [tracker send:[[GAIDictionaryBuilder createAppView] build]];
						 }];
	}
}

-(void)showMessageView
{
	if (activeTextField != nil)
	{
		[activeTextField resignFirstResponder];
	}
	[messageView setAlpha:0.0];
	[messageView setHidden:NO];
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 [currentlyShowingView setAlpha:0.0];
					 }
					 completion:^(BOOL finished) {
						 [UIView animateWithDuration:0.3
											   delay:0
											 options:UIViewAnimationOptionCurveEaseOut
										  animations:^{
											  [messageView setAlpha:1.0];
										  }
										  completion:^(BOOL finished) {
											  [currentlyShowingView setHidden:YES];
											  [currentlyShowingView setAlpha:1.0];
											  [backButton setHidden:NO];
											  currentlyShowingView = messageView;
										  }];
					 }];
}

-(void)showPolicyLogin
{
	if (activeTextField != nil)
	{
		[activeTextField resignFirstResponder];
	}
	[policyView setAlpha:0.0];
	[policyView setHidden:NO];
	[policyGoButton setHidden:NO];
	[policyNotFoundView setHidden:YES];
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 [currentlyShowingView setAlpha:0.0];
					 }
					 completion:^(BOOL finished) {
						 [UIView animateWithDuration:0.3
											   delay:0
											 options:UIViewAnimationOptionCurveEaseOut
										  animations:^{
											  [policyView setAlpha:1.0];
										  }
										  completion:^(BOOL finished) {
											  [currentlyShowingView setHidden:YES];
											  [currentlyShowingView setAlpha:1.0];
											  [backButton setHidden:NO];
											  currentlyShowingView = policyView;
											  id tracker = [[GAI sharedInstance] defaultTracker];
											  [tracker set:kGAIScreenName value:@"Login with Policy Number"];
											  [tracker send:[[GAIDictionaryBuilder createAppView] build]];
										  }];
					 }];
}

-(void)showWelcomeLogin
{
	if (activeTextField != nil)
	{
		[activeTextField resignFirstResponder];
	}
	didFailFirstAttempt = NO;
	didFailSecondAttempt = NO;
	[welcomeView setAlpha:0.0];
	[welcomeView setHidden:NO];
	[UIView animateWithDuration:0.3
						  delay:0
						options:UIViewAnimationOptionCurveEaseIn
					 animations:^{
						 [currentlyShowingView setAlpha:0.0];
						 [backButton setAlpha:0.0];
					 }
					 completion:^(BOOL finished) {
						 [UIView animateWithDuration:0.3
											   delay:0
											 options:UIViewAnimationOptionCurveEaseOut
										  animations:^{
											  [welcomeView setAlpha:1.0];
										  }
										  completion:^(BOOL finished) {
											  [currentlyShowingView setHidden:YES];
											  [currentlyShowingView setAlpha:1.0];
											  [backButton setHidden:YES];
											  [backButton setAlpha:1.0];
											  currentlyShowingView = welcomeView;
										  }];
					 }];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
	activeTextField = textField;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
	activeTextField = nil;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	if (textField == lastNameTextField)
	{
		shouldIgnoreAnimation = YES;
		[lastNameTextField resignFirstResponder];
		[registrationTextField becomeFirstResponder];
	}
	else if (textField == registrationTextField)
	{
		shouldIgnoreAnimation = NO;
		[registrationTextField resignFirstResponder];
	}
	else if (textField == policyNumberTextField)
	{
		shouldIgnoreAnimation = NO;
		[policyNumberTextField resignFirstResponder];
	}
	return YES;
}

- (IBAction)login:(id)sender
{
	[self performSegueWithIdentifier:@"loginSegue" sender:self];
}

-(IBAction)back:(id)sender
{
	[self showWelcomeLogin];
}

-(IBAction)phoneCallCentre:(id)sender
{
	// May return nil if a tracker has not already been initialized with a
	// property ID.
	id tracker = [[GAI sharedInstance] defaultTracker];
	// This screen name value will remain set on the tracker and sent with
	// hits until it is set to a new value or to nil.
	[tracker set:kGAIScreenName value:@"Policy Number Not Found : Click to Call"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	[tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"click-­to-­call"			// Event category (required)
														  action:@"policy-number-not-found"	// Event action (required)
														   label:@"0800 505 511"			// Event label
														   value:nil] build]];
	
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"telprompt://0800505511"]];
}

-(IBAction)loginWithCredentials:(id)sender
{
	shouldIgnoreAnimation = NO;
	if ([lastNameTextField isFirstResponder])
	{
		[lastNameTextField resignFirstResponder];
	}
	else if ([registrationTextField isFirstResponder])
	{
		[registrationTextField resignFirstResponder];
	}
	else if ([policyNumberTextField isFirstResponder])
	{
		[policyNumberTextField resignFirstResponder];
	}
	
	TCAppDelegate *appDelegate = (TCAppDelegate *)[[UIApplication sharedApplication] delegate];
	
	if (didFailFirstAttempt == YES)
	{
		if (didFailSecondAttempt == YES)
		{
			[appDelegate loginThreeWithLastName:lastNameTextField.text
							   withRegistration:registrationTextField.text
							withCompletionBlock:^(BOOL sucess) {
								if (sucess == YES)
								{
									NSLog(@"LOGIN THREE SUCESS");
									if (activeTextField != nil)
									{
										[activeTextField resignFirstResponder];
									}
									[self performSegueWithIdentifier:@"loginSegue" sender:self];
								}
								else
								{
									NSLog(@"LOGIN THREE FAILED");
								}
							}];
		}
		else
		{
			if (policyNumberTextField.text.length > 0)
			{
				[appDelegate loginWithPolicyNumber:policyNumberTextField.text withCompletionBlock:^(BOOL sucess) {
					if (sucess == YES)
					{
						NSLog(@"LOGIN TWO SUCESS");
						if (activeTextField != nil)
						{
							[activeTextField resignFirstResponder];
						}
						[self performSegueWithIdentifier:@"loginSegue" sender:self];
					}
					else
					{
						NSLog(@"LOGIN TWO FAILED");
						didFailSecondAttempt = YES;
						dispatch_async(dispatch_get_main_queue(), ^{
							[self showMessageView];
						});
					}
				}];
			}
			else
			{
				UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Missing Fields"
																	message:@"Please enter an answer into the policy field to login."
																   delegate:nil
														  cancelButtonTitle:@"OK"
														  otherButtonTitles:nil];
				[alertView show];
			}
		}
	}
	else
	{
		if (lastNameTextField.text.length > 0 && registrationTextField.text.length > 0)
		{
			[appDelegate loginWithLastName:lastNameTextField.text
						  withRegistration:registrationTextField.text
							  asynchronous:YES
					   withCompletionBlock:^(BOOL sucess) {
						   if (sucess == YES)
						   {
							   NSLog(@"LOGIN ONE SUCESS");
							   if (activeTextField != nil)
							   {
								   [activeTextField resignFirstResponder];
							   }
							   [self performSegueWithIdentifier:@"loginSegue" sender:self];
						   }
						   else
						   {
							   NSLog(@"LOGIN ONE FAILED");
							   didFailFirstAttempt = YES;
							   dispatch_async(dispatch_get_main_queue(), ^{
								   [self showPolicyLogin];
							   });
							   
						   }
					   }];
		}
		else
		{
			UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Missing Fields"
																message:@"Please enter an answer into both fields to login."
															   delegate:nil
													  cancelButtonTitle:@"OK"
													  otherButtonTitles:nil];
			[alertView show];
		}
	}
}

#pragma mark - Keyboard movement methods
- (void)keyboardWillBeShown:(NSNotification*)aNotification
{
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
	UIViewAnimationCurve keyboardTransitionAnimationCurve;
	[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&keyboardTransitionAnimationCurve];
	NSNumber *kbAD = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
	
	if ((activeTextField.frame.origin.y + activeTextField.frame.size.height) > self.view.frame.size.height - kbSize.height)
	{
		// bottom of text field is below the top of the keyboard.
		//float difference = (self.view.frame.size.height - kbSize.height - activeTextField.frame.origin.y - activeTextField.frame.size.height);
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:[kbAD floatValue]];
		[UIView setAnimationCurve:keyboardTransitionAnimationCurve];
		[self.view setFrame:CGRectMake(0, (0 - kbSize.height), self.view.frame.size.width, self.view.frame.size.height)];
		//[activeTextField setFrame:CGRectMake(activeTextFieldOriginPoint.x, difference, activeTextField.frame.size.width, activeTextField.frame.size.height)];
		[UIView commitAnimations];
	}
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
	if (self.view.frame.origin.y < 0 && shouldIgnoreAnimation != YES)
	{
		NSDictionary* info = [aNotification userInfo];
		UIViewAnimationCurve keyboardTransitionAnimationCurve;
		[[info valueForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&keyboardTransitionAnimationCurve];
		NSNumber *kbAD = [info objectForKey:UIKeyboardAnimationDurationUserInfoKey];
		
		[UIView beginAnimations:nil context:nil];
		[UIView setAnimationDuration:[kbAD floatValue]];
		[UIView setAnimationCurve:keyboardTransitionAnimationCurve];
		[self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
		//[activeTextField setFrame:CGRectMake(activeTextFieldOriginPoint.x, activeTextFieldOriginPoint.y, activeTextField.frame.size.width, activeTextField.frame.size.height)];
		[UIView commitAnimations];
	}
}


@end
