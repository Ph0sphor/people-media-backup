//
//  TCCouponListPopoverViewController.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 15/07/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CouponObject.h"

@protocol TCCouponListPopoverViewControllerDelegate <NSObject>
-(void)didSelectCoupon:(CouponObject *)coupon;
@end

@interface TCCouponListPopoverViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
	IBOutlet UITableView *couponTableView;
}
@property (nonatomic, assign) id<TCCouponListPopoverViewControllerDelegate>delegate;
@property (nonatomic, strong) NSArray *couponArray;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageView;
-(CGFloat)getNewTableHeight;
-(void)reloadData;
@end
