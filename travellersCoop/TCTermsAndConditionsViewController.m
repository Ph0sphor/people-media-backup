//
//  TCTermsAndConditionsViewController.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 11/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCTermsAndConditionsViewController.h"

@interface TCTermsAndConditionsViewController ()

@end

@implementation TCTermsAndConditionsViewController

-(IBAction)back:(id)sender
{
	[self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleDefault;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	id tracker = [[GAI sharedInstance] defaultTracker];
	[tracker set:kGAIScreenName value:@"Terms and Conditions"];
	[tracker send:[[GAIDictionaryBuilder createAppView] build]];
	
	[scrollView setContentSize:contentView.frame.size];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
