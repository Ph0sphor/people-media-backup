//
//  TCCategoryTableViewCell.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 24/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCCategoryTableViewCell.h"

@implementation TCCategoryTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
