//
//  main.m
//  travellersCoop
//
//  Created by Scott Judson on 7/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TCAppDelegate.h"

int main(int argc, char *argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([TCAppDelegate class]));
	}
}
