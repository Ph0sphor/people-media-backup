//
//  TCLoginNoAnimationSegue.m
//  travellersCoop
//
//  Created by Vanessa Taylor on 27/03/14.
//  Copyright (c) 2014 Judson Steel. All rights reserved.
//

#import "TCLoginNoAnimationSegue.h"

@implementation TCLoginNoAnimationSegue
-(void) perform
{
    [[self.sourceViewController navigationController] pushViewController:self.destinationViewController animated:NO];
}
@end
