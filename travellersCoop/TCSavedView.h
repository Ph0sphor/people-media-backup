//
//  TCSavedView.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 23/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreGraphics/CoreGraphics.h>

@interface TCSavedView : UIView
@property (nonatomic, strong) IBOutlet UIImageView *imageView;

-(void)setupImage;
-(UIImage *)maskImage:(UIImage *)theImage withMask:(UIImage *)maskImage;

@end
