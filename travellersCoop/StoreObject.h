//
//  StoreObject.h
//  travellersCoop
//
//  Created by Vanessa Taylor on 18/05/12.
//  Copyright (c) 2012 Judson Steel. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CouponObject;

@interface StoreObject : NSObject

@property (nonatomic, strong) NSString * storeID;
@property (nonatomic, strong) NSString * storeName;
@property (nonatomic, strong) NSString * storePhone;
@property (nonatomic, strong) NSString * storeEmail;
@property (nonatomic, strong) NSString * storeWebsite;
@property (nonatomic, strong) NSString * storeStreetAddress;
@property (nonatomic, strong) NSString * storeSuburb;
@property (nonatomic, strong) NSString * storeCity;
@property (nonatomic, strong) NSString * storePostcode;
@property (nonatomic, strong) NSString * storePincode;
@property (nonatomic, strong) NSString * storeLatitude;
@property (nonatomic, strong) NSString * storeLongitude;
@property (nonatomic, strong) NSMutableSet *coupons;

- (void)addCouponsObject:(CouponObject *)value;
- (void)removeCouponsObject:(CouponObject *)value;

@end
